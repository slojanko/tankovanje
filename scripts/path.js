(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
const Queue = require('./PriorityQueue');

const removeDeepFromMap = require('./removeDeepFromMap');

const toDeepMap = require('./toDeepMap');

const validateDeep = require('./validateDeep');
/** Creates and manages a graph */


class Graph {
  /**
   * Creates a new Graph, optionally initializing it a nodes graph representation.
   *
   * A graph representation is an object that has as keys the name of the point and as values
   * the points reacheable from that node, with the cost to get there:
   *
   *     {
   *       node (Number|String): {
   *         neighbor (Number|String): cost (Number),
   *         ...,
   *       },
   *     }
   *
   * In alternative to an object, you can pass a `Map` of `Map`. This will
   * allow you to specify numbers as keys.
   *
   * @param {Objec|Map} [graph] - Initial graph definition
   * @example
   *
   * const route = new Graph();
   *
   * // Pre-populated graph
   * const route = new Graph({
   *   A: { B: 1 },
   *   B: { A: 1, C: 2, D: 4 },
   * });
   *
   * // Passing a Map
   * const g = new Map()
   *
   * const a = new Map()
   * a.set('B', 1)
   *
   * const b = new Map()
   * b.set('A', 1)
   * b.set('C', 2)
   * b.set('D', 4)
   *
   * g.set('A', a)
   * g.set('B', b)
   *
   * const route = new Graph(g)
   */
  constructor(graph) {
    if (graph instanceof Map) {
      validateDeep(graph);
      this.graph = graph;
    } else if (graph) {
      this.graph = toDeepMap(graph);
    } else {
      this.graph = new Map();
    }
  }
  /**
   * Adds a node to the graph
   *
   * @param {string} name      - Name of the node
   * @param {Object|Map} neighbors - Neighbouring nodes and cost to reach them
   * @return {this}
   * @example
   *
   * const route = new Graph();
   *
   * route.addNode('A', { B: 1 });
   *
   * // It's possible to chain the calls
   * route
   *   .addNode('B', { A: 1 })
   *   .addNode('C', { A: 3 });
   *
   * // The neighbors can be expressed in a Map
   * const d = new Map()
   * d.set('A', 2)
   * d.set('B', 8)
   *
   * route.addNode('D', d)
   */


  addNode(name, neighbors) {
    let nodes;

    if (neighbors instanceof Map) {
      validateDeep(neighbors);
      nodes = neighbors;
    } else {
      nodes = toDeepMap(neighbors);
    }

    this.graph.set(name, nodes);
    return this;
  }
  /**
   * @deprecated since version 2.0, use `Graph#addNode` instead
   */


  addVertex(name, neighbors) {
    return this.addNode(name, neighbors);
  }
  /**
   * Removes a node and all of its references from the graph
   *
   * @param {string|number} key - Key of the node to remove from the graph
   * @return {this}
   * @example
   *
   * const route = new Graph({
   *   A: { B: 1, C: 5 },
   *   B: { A: 3 },
   *   C: { B: 2, A: 2 },
   * });
   *
   * route.removeNode('C');
   * // The graph now is:
   * // { A: { B: 1 }, B: { A: 3 } }
   */


  removeNode(key) {
    this.graph = removeDeepFromMap(this.graph, key);
    return this;
  }
  /**
   * Compute the shortest path between the specified nodes
   *
   * @param {string}  start     - Starting node
   * @param {string}  goal      - Node we want to reach
   * @param {object}  [options] - Options
   *
   * @param {boolean} [options.trim]    - Exclude the origin and destination nodes from the result
   * @param {boolean} [options.reverse] - Return the path in reversed order
   * @param {boolean} [options.cost]    - Also return the cost of the path when set to true
   *
   * @return {array|object} Computed path between the nodes.
   *
   *  When `option.cost` is set to true, the returned value will be an object with shape:
   *    - `path` *(Array)*: Computed path between the nodes
   *    - `cost` *(Number)*: Cost of the path
   *
   * @example
   *
   * const route = new Graph()
   *
   * route.addNode('A', { B: 1 })
   * route.addNode('B', { A: 1, C: 2, D: 4 })
   * route.addNode('C', { B: 2, D: 1 })
   * route.addNode('D', { C: 1, B: 4 })
   *
   * route.path('A', 'D') // => ['A', 'B', 'C', 'D']
   *
   * // trimmed
   * route.path('A', 'D', { trim: true }) // => [B', 'C']
   *
   * // reversed
   * route.path('A', 'D', { reverse: true }) // => ['D', 'C', 'B', 'A']
   *
   * // include the cost
   * route.path('A', 'D', { cost: true })
   * // => {
   * //       path: [ 'A', 'B', 'C', 'D' ],
   * //       cost: 4
   * //    }
   */


  path(start, goal, options = {}) {
    // Don't run when we don't have nodes set
    if (!this.graph.size) {
      if (options.cost) return {
        path: null,
        cost: 0
      };
      return null;
    }

    const explored = new Set();
    const frontier = new Queue();
    const previous = new Map();
    let path = [];
    let totalCost = 0;
    let avoid = [];
    if (options.avoid) avoid = [].concat(options.avoid);

    if (avoid.includes(start)) {
      throw new Error(`Starting node (${start}) cannot be avoided`);
    } else if (avoid.includes(goal)) {
      throw new Error(`Ending node (${goal}) cannot be avoided`);
    } // Add the starting point to the frontier, it will be the first node visited


    frontier.set(start, 0); // Run until we have visited every node in the frontier

    while (!frontier.isEmpty()) {
      // Get the node in the frontier with the lowest cost (`priority`)
      const node = frontier.next(); // When the node with the lowest cost in the frontier in our goal node,
      // we can compute the path and exit the loop

      if (node.key === goal) {
        // Set the total cost to the current value
        totalCost = node.priority;
        let nodeKey = node.key;

        while (previous.has(nodeKey)) {
          path.push(nodeKey);
          nodeKey = previous.get(nodeKey);
        }

        break;
      } // Add the current node to the explored set


      explored.add(node.key); // Loop all the neighboring nodes

      const neighbors = this.graph.get(node.key) || new Map();
      neighbors.forEach((nCost, nNode) => {
        // If we already explored the node, or the node is to be avoided, skip it
        if (explored.has(nNode) || avoid.includes(nNode)) return null; // If the neighboring node is not yet in the frontier, we add it with
        // the correct cost

        if (!frontier.has(nNode)) {
          previous.set(nNode, node.key);
          return frontier.set(nNode, node.priority + nCost);
        }

        const frontierPriority = frontier.get(nNode).priority;
        const nodeCost = node.priority + nCost; // Otherwise we only update the cost of this node in the frontier when
        // it's below what's currently set

        if (nodeCost < frontierPriority) {
          previous.set(nNode, node.key);
          return frontier.set(nNode, nodeCost);
        }

        return null;
      });
    } // Return null when no path can be found


    if (!path.length) {
      if (options.cost) return {
        path: null,
        cost: 0
      };
      return null;
    } // From now on, keep in mind that `path` is populated in reverse order,
    // from destination to origin
    // Remove the first value (the goal node) if we want a trimmed result


    if (options.trim) {
      path.shift();
    } else {
      // Add the origin waypoint at the end of the array
      path = path.concat([start]);
    } // Reverse the path if we don't want it reversed, so the result will be
    // from `start` to `goal`


    if (!options.reverse) {
      path = path.reverse();
    } // Return an object if we also want the cost


    if (options.cost) {
      return {
        path,
        cost: totalCost
      };
    }

    return path;
  }
  /**
   * @deprecated since version 2.0, use `Graph#path` instead
   */


  shortestPath(...args) {
    return this.path(...args);
  }

}

module.exports = Graph;

},{"./PriorityQueue":2,"./removeDeepFromMap":3,"./toDeepMap":4,"./validateDeep":5}],2:[function(require,module,exports){
/**
 * This very basic implementation of a priority queue is used to select the
 * next node of the graph to walk to.
 *
 * The queue is always sorted to have the least expensive node on top.
 * Some helper methods are also implemented.
 *
 * You should **never** modify the queue directly, but only using the methods
 * provided by the class.
 */
class PriorityQueue {
  /**
   * Creates a new empty priority queue
   */
  constructor() {
    // The `keys` set is used to greatly improve the speed at which we can
    // check the presence of a value in the queue
    this.keys = new Set();
    this.queue = [];
  }
  /**
   * Sort the queue to have the least expensive node to visit on top
   *
   * @private
   */


  sort() {
    this.queue.sort((a, b) => a.priority - b.priority);
  }
  /**
   * Sets a priority for a key in the queue.
   * Inserts it in the queue if it does not already exists.
   *
   * @param {any}     key       Key to update or insert
   * @param {number}  value     Priority of the key
   * @return {number} Size of the queue
   */


  set(key, value) {
    const priority = Number(value);
    if (isNaN(priority)) throw new TypeError('"priority" must be a number');

    if (!this.keys.has(key)) {
      // Insert a new entry if the key is not already in the queue
      this.keys.add(key);
      this.queue.push({
        key,
        priority
      });
    } else {
      // Update the priority of an existing key
      this.queue.map(element => {
        if (element.key === key) {
          Object.assign(element, {
            priority
          });
        }

        return element;
      });
    }

    this.sort();
    return this.queue.length;
  }
  /**
   * The next method is used to dequeue a key:
   * It removes the first element from the queue and returns it
   *
   * @return {object} First priority queue entry
   */


  next() {
    const element = this.queue.shift(); // Remove the key from the `_keys` set

    this.keys.delete(element.key);
    return element;
  }
  /**
   * @return {boolean} `true` when the queue is empty
   */


  isEmpty() {
    return Boolean(this.queue.length === 0);
  }
  /**
   * Check if the queue has a key in it
   *
   * @param {any} key   Key to lookup
   * @return {boolean}
   */


  has(key) {
    return this.keys.has(key);
  }
  /**
   * Get the element in the queue with the specified key
   *
   * @param {any} key   Key to lookup
   * @return {object}
   */


  get(key) {
    return this.queue.find(element => element.key === key);
  }

}

module.exports = PriorityQueue;

},{}],3:[function(require,module,exports){
/**
 * Removes a key and all of its references from a map.
 * This function has no side-effects as it returns
 * a brand new map.
 *
 * @param {Map}     map - Map to remove the key from
 * @param {string}  key - Key to remove from the map
 * @return {Map}    New map without the passed key
 */
function removeDeepFromMap(map, key) {
  const newMap = new Map();

  for (const [aKey, val] of map) {
    if (aKey !== key && val instanceof Map) {
      newMap.set(aKey, removeDeepFromMap(val, key));
    } else if (aKey !== key) {
      newMap.set(aKey, val);
    }
  }

  return newMap;
}

module.exports = removeDeepFromMap;

},{}],4:[function(require,module,exports){
/**
 * Validates a cost for a node
 *
 * @private
 * @param {number} val - Cost to validate
 * @return {bool}
 */
function isValidNode(val) {
  const cost = Number(val);

  if (isNaN(cost) || cost <= 0) {
    return false;
  }

  return true;
}
/**
 * Creates a deep `Map` from the passed object.
 *
 * @param  {Object} source - Object to populate the map with
 * @return {Map} New map with the passed object data
 */


function toDeepMap(source) {
  const map = new Map();
  const keys = Object.keys(source);
  keys.forEach(key => {
    const val = source[key];

    if (val !== null && typeof val === 'object' && !Array.isArray(val)) {
      return map.set(key, toDeepMap(val));
    }

    if (!isValidNode(val)) {
      throw new Error(`Could not add node at key "${key}", make sure it's a valid node`, val);
    }

    return map.set(key, Number(val));
  });
  return map;
}

module.exports = toDeepMap;

},{}],5:[function(require,module,exports){
/**
 * Validate a map to ensure all it's values are either a number or a map
 *
 * @param {Map} map - Map to valiadte
 */
function validateDeep(map) {
  if (!(map instanceof Map)) {
    throw new Error(`Invalid graph: Expected Map instead found ${typeof map}`);
  }

  map.forEach((value, key) => {
    if (typeof value === 'object' && value instanceof Map) {
      validateDeep(value);
      return;
    }

    if (typeof value !== 'number' || value <= 0) {
      throw new Error(`Values must be numbers greater than 0. Found value ${value} at ${key}`);
    }
  });
}

module.exports = validateDeep;

},{}],6:[function(require,module,exports){
const Graph = require('node-dijkstra');

var orig_field;
var field;

var corners;
var cornerGraph;

scale = 1;

maxL = 10;

function increaseField(f){
  newf = [];

  c = 0;
  for(var i = 0; i < f.length; i++){
    newf.push([]);
    newf.push([]);
    for(var j = 0; j < f[0].length; j++){

        newf[c].push(f[i][j]);
        newf[c].push(f[i][j]);

        newf[c + 1].push(f[i][j]);
        newf[c + 1].push(f[i][j]);

      }
      c += 2;
  }
  return newf;
}
function increaseField2(f){
  newf = [];

  c = 0;
  for(var i = 0; i < f.length; i++){
    newf.push([]);
    newf.push([]);
    for(var j = 0; j < f[0].length; j++){

        newf[c].push(f[i][j]);
        newf[c].push(f[i][j]);

        newf[c + 1].push(f[i][j]);
        newf[c + 1].push(f[i][j]);
      }
      c += 2;
  }

  final_f = [];
  for(var i = 0; i < newf.length; i++){
    final_f.push([]);
    for(var j = 0; j < newf[0].length; j++){
        if(i == 0 || j == 0 || i == newf.length - 1 || j == newf[0].length - 1 || newf[i][j] == 2){
          final_f[i].push(newf[i][j]);
        }
        else{
          final_f[i].push(0);
        }
      }
  }

  for(var i = 1; i < newf.length - 1; i++){
    for(var j = 1; j < newf[0].length - 1; j++){
        if(newf[i][j] == 1){
          final_f[i][j] = 1;
          if(newf[i][j + 1] == 1 && newf[i - 1][j] == 1 && newf[i + 1][j] != 1 && newf[i][j - 1] != 1){
            final_f[i + 1][j - 1] = 1;
            final_f[i + 1][j] = 1;
            final_f[i][j - 1] = 1;
          }
          if(newf[i][j + 1] != 1 && newf[i - 1][j] != 1 && newf[i + 1][j] == 1 && newf[i][j - 1] == 1){
            final_f[i - 1][j + 1] = 1;
            final_f[i][j + 1] = 1;
            final_f[i - 1][j] = 1;
          }
          if(newf[i][j + 1] != 1 && newf[i - 1][j] == 1 && newf[i + 1][j] != 1 && newf[i][j - 1] == 1){
            final_f[i + 1][j + 1] = 1;
            final_f[i][j + 1] = 1;
            final_f[i + 1][j] = 1;
          }
          if(newf[i][j + 1] == 1 && newf[i - 1][j] != 1 && newf[i + 1][j] == 1 && newf[i][j - 1] != 1){
            final_f[i - 1][j - 1] = 1;
            final_f[i - 1][j] = 1;
            final_f[i][j - 1] = 1;
          }
          //final_f[i - 1][j] = 1;
          //final_f[i + 1][j] = 1;
          //final_f[i][j - 1] = 1;
          //final_f[i][j + 1] = 1;
        }
      }
  }
  return final_f;
}

function printArray(arr){
  arrText = '';
  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr[i].length; j++) {
        arrText+=arr[i][j]+' ';
    }
    console.log(arrText);
    arrText='';
  }
}

function getRectangleCorner(cord_x, cord_y, center_x, center_y, angle){
  tempX = cord_x - center_x;
  tempY = cord_y - center_y;

  rotatedX = tempX * Math.cos(angle) - tempY * Math.sin(angle);
  rotatedY = tempX * Math.sin(angle) + tempY * Math.cos(angle);

  return [rotatedX + center_x, rotatedY + center_y];
}
function getNewAngle(x, y, angle){
  x = x * scale;
  y = y * scale;

  a = angle * (Math.PI / 180);

  x = Math.round(x);
  y = Math.round(y);

  dif = 20;

  mod = ((angle % 360)+ 360 ) % 360;

  moves_right = (Math.cos(a) >= 1 - 0.5);
  moves_left = (Math.cos(a) <= - 1 + 0.5);
  moves_up = (Math.sin(a) >= 1 - 0.5);
  moves_down = (Math.sin(a) <= - 1 + 0.5);

  if(isTree(x, y)){
    return 179 + angle;
  }

  bools = [moves_down, moves_left, moves_right, moves_up];
  count = 0;

  for(var i = 0; i < bools.length; i++){
    if(bools[i]){
      count++;
    }
  }

  if(count == 1){
    if(moves_left && field[x][y - 1] == 1 || moves_right && field[x][y + 1] == 1 || moves_down && field[x + 1][y] == 1 || moves_up && field[x - 1][y] == 1)
      return -1;
    if(moves_left && field[x][y - 1] != 1){
      return 180;
    } 
    if(moves_right && field[x][y + 1] != 1){
      return 0;
    }
    if(moves_down && field[x + 1][y] != 1){
     return 90; 
    }
    if(moves_up && field[x - 1][y] != 1){
      return -90;
    }
  }
  if(count == 2){
    if(moves_left && moves_up && isTree(x - 1, y - 1) || moves_right && moves_up && isTree(x - 1, y + 1) || moves_left && moves_down && isTree(x + 1, y - 1) || moves_right && moves_down && isTree(x + 1, y + 1)){
      
    }
  }

  if(moves_up && moves_right && !isWall(x - 1, y) && !isWall(x, y + 1)){
    if(field[x + 1][y + 1] == 1){
      return 181 + angle;
    }
    return angle;
  }
  if(moves_up && moves_left && !isWall(x - 1, y) && !isWall(x, y - 1)){
    if(field[x - 1][y + 1] == 1){
      return angle;
    }
    return 181 + angle;
  }
  if(moves_down && moves_right && !isWall(x + 1, y) && !isWall(x, y + 1)){
    if(field[x + 1][y - 1] == 1){
      return angle;
    }
    return 181 + angle;
  }
  if(moves_down && moves_left && !isWall(x + 1, y) && !isWall(x, y - 1)){
    if(field[x - 1][y - 1] == 1){
      return 181 + angle;
    }
    return angle;
  }
  if(moves_left && field[x][y - 1] != 1 && field[x][y + 1] == 1){
    return angle;
  }
  if(moves_right && field[x][y + 1] != 1 && field[x][y - 1] == 1){
    return angle;
  }
  if(moves_up && field[x - 1][y] != 1 && field[x + 1][y] == 1){
    return 181 + angle;
  }
  if(moves_down && field[x + 1][y] != 1 && field[x - 1][y] == 1){
    return 181 + angle;
  }
  mod = -mod;
  if(field[x + 1][y + 1] == 1 && mod > 305 - dif && mod < 305 + dif){
    return 181 + angle;
  }
  if(field[x + 1][y - 1] == 1 && mod > 225 - dif && mod < 225 + dif){
    return 181 + angle;
  }
  if(field[x - 1][y - 1] == 1 && mod > 135 - dif && mod < 135 + dif){
    return 181 + angle;
  }
  if(field[x - 1][y + 1] == 1 && mod > 45 - dif && mod < 45 + dif){
    return 181 + angle;
  }

  if(moves_up && moves_right && field[x - 1][y] != 1){
    return 270;
  }
  if(moves_up && moves_right && field[x][y + 1] != 1){
    return 0
  }
  if(moves_down && moves_right && field[x + 1][y] != 1){
    return 90;
  }
  if(moves_down && moves_right && field[x][y + 1] != 1){
    return 0;
  }
  if(moves_up && moves_left && field[x - 1][y] != 1){
    return 270;
  }
  if(moves_up && moves_left && field[x][y - 1] != 1){
    return 180;
  }
  if(moves_down && moves_left && field[x + 1][y] != 1){
    return 90;
  }
  if(moves_down && moves_left && field[x][y - 1] != 1){
    return 180;
  }
  else{
    if(moves_left || moves_right){
      return 180 + angle;
    }
    else{
      return angle;
    }
  }
}
function isTree(x, y){
  for(var i = x - 1; i <= x + 1; i++){
    for(var j = y - 1; j <= y + 1; j++){
      if((i != x || j != y) && i > 0 && i < field.length && j > 0 && j < field[0].length && field[i][j] == 1){
        return false;
      }
    }
  }
  return field[x][y] == 1;
}
function isWall(x, y){
  for(var i = x - 1; i <= x + 1; i++){
    for(var j = y - 1; j <= y + 1; j++){
      if((i != x || j != y) && i > 0 && i < field.length && j > 0 && j < field[0].length && field[i][j] == 1){
        return field[x][y] == 1;
      }
    }
  }
  return false;
}
function treeCheck(x, y, angle1, angle2, collision){
  x = Math.round(x);
  y = Math.round(y);

  moves_right = (Math.cos(a) >= 1 - 0.5);
  moves_left = (Math.cos(a) <= - 1 + 0.5);
  moves_up = (Math.sin(a) >= 1 - 0.5);
  moves_down = (Math.sin(a) <= - 1 + 0.5);

  if(isTree(x, y)){
    a = angle1 * (Math.PI / 180);
    moves_right = (Math.cos(a) >= 1 - 0.5);
    moves_left = (Math.cos(a) <= - 1 + 0.5);
    moves_up = (Math.sin(a) >= 1 - 0.5);
    moves_down = (Math.sin(a) <= - 1 + 0.5);
    if((moves_right && moves_up)){
      if((collision == 2)){
        return -1;
      }
      if(collision == 5){
        return -90;
      }
      if(collision == 1){
        return 0;
      }
      return -angle1;
    }
    if((moves_left && moves_up)){
      if(collision == 0){
        return -1;
      }
      if(collision == 1){
        return 180;
      }
      if(collision == 3){
        return -90;
      }
      return -angle1;
    }
    if((moves_right && moves_down)){
      if(collision == 8){
        return -1;
      }
      if(collision == 5){
        return 90;
      }
      if(collision == 7){
        return 0;
      }
      return -angle1;
    }
    if((moves_left && moves_down)){
      if(collision == 6){
        return -1;
      }
      if(collision == 3){
        return 90;
      }
      if(collision == 7){
        return 180;
      }
      return -angle1;
    }
    if((moves_right || moves_up) && (collision == 2 || collision == 1 || collision == 5)){
      //return -1;
    }
    if((moves_left || moves_up) && (collision == 0 || collision == 1 || collision == 3)){
      //return -1;
    }
    if((moves_right || moves_down) && (collision == 8 || collision == 5 || collision == 7)){
      //return -1;
    }
    if((moves_left || moves_down) && (collision == 6 || collision == 3 || collision == 7)){
      //return -1;
    }
    return 179 + angle1;
  }
  return angle2;
}
function collides(x, y, width, height, angle, isBullet){
  if(x < 1 || x > field.length - 2 || y < 1 || y > field[0].length - 2){
    if(isBullet){
      return 10;
    }
  }
  player_x = x;
  player_y = y;
  player_pos_save = 0;

  width = width * scale;
  height = height * scale;
  x = x * scale;
  y = y * scale;
  angle = angle * (Math.PI / 180);

  C1 = getRectangleCorner(y + width, x + height, y, x, angle);
  C2 = getRectangleCorner(y + width, x - height, y, x, angle);
  C3 = getRectangleCorner(y - width, x - height, y, x, angle);
  C4 = getRectangleCorner(y - width, x + height, y, x, angle);

  cornrs = [C1, C2, C3, C4];

  top_right = [0, 0];
  bottom_right = [0, 0];
  bottom_left = [0, 0];
  top_left = [0, 0];

  min = Infinity;
  for(var i = 0; i < cornrs.length; i++){
    dist = euclideanDistance(y + width, x + width, cornrs[i][0], cornrs[i][1]);
    if(dist < min){
      min = dist;
      top_right = cornrs[i];
    }
  }

  min = Infinity;
  for(var i = 0; i < cornrs.length; i++){
    dist = euclideanDistance(y + width, x - width, cornrs[i][0], cornrs[i][1]);
    if(dist < min){
      min = dist;
      bottom_right = cornrs[i];
    }
  }

  min = Infinity;
  for(var i = 0; i < cornrs.length; i++){
    dist = euclideanDistance(y - width, x - width, cornrs[i][0], cornrs[i][1]);
    if(dist < min){
      min = dist;
      bottom_left = cornrs[i];
    }
  }

  min = Infinity;
  for(var i = 0; i < cornrs.length; i++){
    dist = euclideanDistance(y - width, x + width, cornrs[i][0], cornrs[i][1]);
    if(dist < min){
      min = dist;
      top_left = cornrs[i];
    }
  }

  corners_ = [[y, x], top_right, bottom_right, bottom_left, top_left];

  x = Math.round(x);
  y = Math.round(y);

  W = 0.5 * scale;
  T = 0.15 * scale;

  if(field[x - 1][y] == 1){
    for(var k = 0; k < corners_.length; k++){
      cor_x = corners_[k][0];
      cor_y = corners_[k][1];
      object_width = W;
      if(isTree(x - 1, y)){
        object_width = T;
        if(k == 0){
          object_width += width;
        }
      }
      else if(k == 0){
        object_width += height;
      }
      if(cor_x >= y - object_width && cor_x <= y + object_width && cor_y >= x - 1 - object_width && cor_y <= x - 1 + object_width){
        return 1;
      }
    }
  }

  if(field[x][y - 1] == 1){
    for(var k = 0; k < corners_.length; k++){
      cor_x = corners_[k][0];
      cor_y = corners_[k][1];
      object_width = W;
      if(isTree(x, y - 1)){
        object_width = T;
        if(k == 0){
          object_width += width;
        }
      }
      else if(k == 0){
        object_width += height;
      }
      if(cor_x >= y - 1 - object_width && cor_x <= y - 1 + object_width && cor_y >= x - object_width && cor_y <= x + object_width){
        return 3;
      }
    }
  }
  if(field[x][y + 1] == 1){
    for(var k = 0; k < corners_.length; k++){
      cor_x = corners_[k][0];
      cor_y = corners_[k][1];
      object_width = W;
      if(isTree(x, y + 1)){
        object_width = T;
        if(k == 0){
          object_width += width;
        }
      }
      else if(k == 0){
        object_width += height;
      }
      if(cor_x >= y + 1 - object_width && cor_x <= y + 1 + object_width && cor_y >= x - object_width && cor_y <= x + object_width){
        return 5;
      }
    }
  }
  
  if(field[x + 1][y] == 1){
    for(var k = 0; k < corners_.length; k++){
      cor_x = corners_[k][0];
      cor_y = corners_[k][1];
      object_width = W;
      if(isTree(x + 1, y)){
        object_width = T;
        if(k == 0){
          object_width += width;
        }
      }
      else if(k == 0){
        object_width += height;
      }
      if(cor_x >= y - object_width && cor_x <= y + object_width && cor_y >= x + 1 - object_width && cor_y <= x + 1 + object_width){
        return 7;
      }
    }
  }
  

  count = 0;
  for(var i = x - 1; i <= x + 1; i++){
    for(var j = y - 1; j <= y + 1; j++){
      if(field[i][j] == 1){
        for(var k = 0; k < corners_.length; k++){
          cor_x = corners_[k][0];
          cor_y = corners_[k][1];
          object_width = W;
          if(isTree(i, j)){
            object_width = T;
            if(k == 0){
              object_width += width;
            }
          }
          else if(k == 0){
            object_width += height;
          }
          if(cor_x >= j - object_width && cor_x <= j + object_width && cor_y >= i - object_width && cor_y <= i + object_width){
            return count;
          }
        }
      }
      count++;
    }
  }
  return -1;
}
function cornerizeField(field){
    for(var i = 1; i < field.length - 1; i++){
        for(var j = 1; j < field[i].length - 1; j++){
            if(field[i][j] == 1){
                // Check upper left corner
                if(field[i + 1][j] == 0 && field[i][j - 1] == 0 & field[i + 1][j - 1] == 0){
                    field[i + 1][j - 1] = 2
                }
                // Check upper right corner
                if(field[i + 1][j] == 0 && field[i][j + 1] == 0 & field[i + 1][j + 1] == 0){
                    field[i + 1][j + 1] = 2
                }
                // Check lower left corner
                if(field[i - 1][j] == 0 && field[i][j - 1] == 0 & field[i - 1][j - 1] == 0){
                    field[i - 1][j - 1] = 2
                }
                // Check lower right corner
                if(field[i - 1][j] == 0 && field[i][j + 1] == 0 & field[i - 1][j + 1] == 0){
                    field[i - 1][j + 1] = 2
                }
            }
        }
    }
}

function euclideanDistance(x1, y1, x2, y2){
    return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
}

function separation(x, y, locations, radius, maxSeparation){
  var force = [0, 0, 0];
  var neighborCount = 0;

  for (var i = 0; i < locations.length; i++) {
      var location = locations[i];
      var loc_x = location[0];
      var loc_y = location[1];

      if (euclideanDistance(b, this) <= radius) {
          force[0] += loc_x - x;
          force[1] += loc_y - y;
          neighborCount++;
      }
  }

  if (neighborCount != 0) {
      force[0] /= neighborCount;
      force[1] /= neighborCount;

      force[0] *= -1;
      force[1] *= -1;
  }

  force_length = Math.sqrt((force[0] * force[0]) + (force[1] * force[1]) + (force[2] * force[2]));

  force[0] /= force_length;
  force[1] /= force_length;
  force[2] /= force_length;

  force[0] *= maxSeparation;
  force[1] *= maxSeparation;
  force[2] *= maxSeparation;

  return force;
}

function getCornerGraph(field){
    graph = [];
    count = 0;
    for(var i = 0; i < field.length; i++){
        for(var j = 0; j < field[i].length; j++){
            if(field[i][j] == 2){
                graph.push([i, j]);
            }
        }
    }

    for(var i = 0; i < graph.length; i++){
        for(var j = i + 1; j < graph.length; j++){
            if (crossesWall([graph[i][0], graph[i][1]],[graph[j][0], graph[j][1]], field) == false){
                var distance = euclideanDistance(graph[i][0], graph[i][1], graph[j][0], graph[j][1]);
                graph[i].push([graph[j][0], graph[j][1], distance, j]);
                graph[j].push([graph[i][0], graph[i][1], distance, i]);
            }
        }
    }
 
    const route = new Graph()

    for(var i = 0; i < graph.length; i++){
        var dict = {};
        for(var j = 2; j < graph[i].length; j++){
            keyName = graph[i][j][0] + " " + graph[i][j][1];
            dict[keyName] = graph[i][j][2];
        }
        
    route.addNode(graph[i][0] + " " + graph[i][1], dict);
    }
 
    return [graph, route];
}
/*
    * Given 2 points (x1, y1) and (x2, y2) returns the line equation in the form of a [slope, c] pair
*/
function getLine(x1, y1, x2, y2){
    var slope = (y2 - y1) / (x2 - x1);
    var c = (y2 - (slope * x2));
    return [slope, c];
}

/*
    * Given 2 points (p1 and p2) and a field where walls are represented with the value 1, detects
    * whether the line between the given points crosses a wall
    * example crossesWall([1, 9], [5, 12], field)
*/
function crossesWall(p1, p2, field){
    var x1 = p1[0];
    var y1 = p1[1];

    var x2 = p2[0];
    var y2 = p2[1];

    if(euclideanDistance(x1, y1, x2, y2) == euclideanDistance(1,1,2,2)){
      return false;
    }

    // Gets only the part of the field we are interested in - to prevent searching the whole field
    var subField = getSubField(field, p1, p2);

    // If the line is not a vertical one we proceed normally
    if(y1 != y2){
        xOrigin = Math.min(x1, x2);
        yOrigin = Math.min(y1, y2);

        x1 = x1 - xOrigin;
        x2 = x2 - xOrigin;

        y1 = y1 - yOrigin;
        y2 = y2 - yOrigin;

        xFin    = Math.max(x1, x2);
        yFin    = Math.max(y1, y2);

        // We get the line equation
        line = getLine(y1, x1, y2, x2);

        // For every point in the field
        for (var i = 0; i <= xFin; i++){
            for (var j = 0; j <= yFin; j++){
                box_up_x = i + 0.5;
                box_down_x = i - 0.5;
                box_left_y = j - 0.5;
                box_right_y = j + 0.5;
                
                m = line[0];
                c = line[1];

                // Check if the y on the left side of the box returns an x which is in between the min and max x of the box
                if(m * box_left_y + c >= box_down_x && m * box_left_y + c <= box_up_x){
                    if(subField[i][j] == 1) return true;
                }
                // Check if the y on the right side of the box returns an x which is in between the min and max x of the box
                if(m * box_right_y + c >= box_down_x && m * box_right_y + c <= box_up_x){
                    if(subField[i][j] == 1) return true;
                }
                // Check if the x on the upper side of the box returns an y which is in between the min and max y of the box
                if((box_up_x - c) / m >= box_left_y && (box_up_x - c) / m <= box_right_y){
                    if(subField[i][j] == 1) return true;
                }
                // NOTE: We do not test the lower side of the box, 3 sides are enough, since a line cannot cross only 1 side at a time
            }
        }
    }
    // The line is vertical
    else{
        // The whole subField (1D vertical vector) is being crossed
        for(var i = 0; i < subField.length; i++) {
            if(subField[i][0] == 1) return true;
        }
    }

    return false;
}
/*
    * Gets the square sub-field of a field bounded by the points p1 and p2
    * example: getSubField(field, [7, 10] , [2, 8])
*/
function getSubField(field, p1, p2){
    var x1 = p1[0];
    var y1 = p1[1];

    var x2 = p2[0];
    var y2 = p2[1];

    var pointsOfInterest = [];

    for(var i = Math.min(x1, x2); i <= Math.max(x1, x2); i++){
        var newRow = [];
        for(var j = Math.min(y1, y2); j <= Math.max(y1, y2); j++){
            newRow.push(field[i][j]);
        }
        pointsOfInterest.push(newRow);
    }

    return pointsOfInterest;
}
function getPath(cornerGraph, x1, y1, x2, y2){
    if (x1 == x2 && y1 == y2){
      return [x1, y1];
    }
    p = cornerGraph.path(x1 + " " + y1, x2 + " " + y2);

    path = []
    for(var i = 0; i < p.length; i++){
        path.push(p[i].split(' ').map(function(val) {
            return parseInt(val, 10);
        })
        )
    }

    return path;
}

function getPathLength(path){
    distance = 0;

    for(var i = 0; i < path.length - 1; i++){
        x_p = path[i][0];
        y_p = path[i][1];

        x_n = path[i + 1][0];
        y_n = path[i + 1][1];

        distance += euclideanDistance(x_p, y_p, x_n, y_n);
    }

    return distance;
}

function getNewField(positions){
    field = [];

    for(var i = 0; i < orig_field.length; i++){
      field.push([]);
      for(var j = 0; j < orig_field[0].length; j++){
        field[i].push(orig_field[i][j]);
      }
    }

    for(var k = 0; k < positions.length; k++){
      var bot_X_cord = Math.round(positions[k][0] * scale);
      var bot_Y_cord = Math.round(positions[k][1] * scale);

      field[bot_X_cord][bot_Y_cord] = 1;

      changed = 3;

      if(orig_field[bot_X_cord][bot_Y_cord] == 2){
        changed = 5;
      }

      for(i = bot_X_cord - 1; i <= bot_X_cord + 1; i++){
        for(j = bot_Y_cord - 1; j <= bot_Y_cord + 1; j++){
          if(field[i][j] == 0 && changed > 0){
              field[i][j] = 2;
              changed--;
          }
        }
      }
    }
    [corners, cornerGraph] = getCornerGraph(field);
}

function findPlayer(botPositionX, botPositionY, playerPositionX, playerPositionY){
    //getPath(cornerGraph, 2, 2, 7, 17);

    var minDistance = Infinity;

    // Get the nearest corner for the player
    for(var i = 0; i < corners.length; i++){
        cor_x = corners[i][0];
        cor_y = corners[i][1];
        distance = euclideanDistance(playerPositionX, playerPositionY, cor_x, cor_y);

        if(crossesWall([cor_x, cor_y], [Math.round(playerPositionX), Math.round(playerPositionY)], field) == false && distance < minDistance){
            minDistance = distance;
            closest_x_player = cor_x;
            closest_y_player = cor_y;
        }
    }

    minDistance = Infinity;
    // Get the best corner for the bot to visit next (not necesarrily the closest one)
    for(var i = 0; i < corners.length; i++){
        cor_x = corners[i][0];
        cor_y = corners[i][1];

        if(crossesWall([cor_x, cor_y], [Math.round(botPositionX), Math.round(botPositionY)], field) == false){
            // distance = distance to corner + distance of graph path
           
            l = euclideanDistance(botPositionX, botPositionY, cor_x, cor_y);
            if(l < maxL){
              path = getPath(cornerGraph, cor_x, cor_y, closest_x_player, closest_y_player);
              if(path[0].length == null){
                v1 = path[0];
                v2 = path[1];
                path = [];
                path.push([v1, v2]);
              }
              distance = l + getPathLength(path);

              if(distance < minDistance){
                  minDistance = distance;
                  best_x_bot = cor_x;
                  best_y_bot = cor_y;
                  best_path = path;
                }
                maxL = 2;
                break;
            }
            
        }
    }

    best_path = [[botPositionX, botPositionY]].concat(best_path);

    final_path = [];
    for(var i = 0; i < best_path.length; i++){
        x = best_path[i][0];
        y = best_path[i][1];

        final_path.push([x / scale - 1, y / scale - 1]);

        if(crossesWall([Math.round(x), Math.round(y)], [Math.round(botPositionX), Math.round(botPositionY)], field) == false && (x != botPositionX || y != botPositionY)){
          final_path = [];
          final_path.push([[botPositionX / scale - 1, botPositionY / scale - 1]])
          final_path.push([x / scale - 1, y / scale - 1]);
      }

        // If you can see the player from x,y directly (meaning there is no wall in between)
        // remove the rest of the path and go there directly
        if(crossesWall([Math.round(x), Math.round(y)], [Math.round(playerPositionX), Math.round(playerPositionY)], field) == false){
            final_path.push([playerPositionX / scale - 1, playerPositionY / scale - 1])
            break;
        }
    }

    return final_path;
}

function printPath(path){
    for (var i = 0; i < path.length; i++){
        console.log(path[i][0] + ", " + path[i][1])
    }
}

function check(x, y){
  return field[x][y] != 1;
}

function init(obj, f){
    orig_field = f;
    cornerizeField(orig_field);
    // orig_field = increaseField2(orig_field);
    [corners, cornerGraph] = getCornerGraph(orig_field);
    //printArray(orig_field);
    getNewField([]);
}

function pathfinding(){this.field = field;}
pathfinding.prototype.init = function(f){init(this, f)};
//pathfinding.prototype.findPlayer = function(){return findPlayer(this.botX, this.botY, this.playerX, this.playerY, this.cornerGraph, this.corners);};
pathfinding.prototype.findPlayer = function(botX, botY, playerX, playerY){return findPlayer((botX + 1) * scale, (botY + 1) * scale, (playerX + 1) * scale, (playerY + 1) * scale);};
pathfinding.prototype.printPath = function(p){printPath(p)};
pathfinding.prototype.getNewField = function(locations){getNewField(locations)};
pathfinding.prototype.collides = function(x, y, width, height, angle, isBullet){return collides(x + 1, y + 1, width, height, angle, isBullet)};
pathfinding.prototype.getNewAngle = function(x, y, angle){return getNewAngle(x + 1, y + 1, angle)};
pathfinding.prototype.treeCheck = function(x, y, angle1, angle2){return treeCheck(x + 1, y + 1, angle1, angle2, collision)};
pathfinding.prototype.crossesWall = function(x1, y1, x2, y2){return crossesWall([Math.round(x1 + 1), Math.round(y1 + 1)], [Math.round(x2 + 1), Math.round(y2 + 1)], field)};
pathfinding.prototype.check = function(x, y){return check(x + 1, y + 1)};

window.pathfinding = pathfinding;
//pf2 = new pathfinding();
//pf2.init()

//pf.botX = 8.3;
//pf.botY = 1.1;
//pf.playerX = 6.7;
//pf.playerY = 9.9;
//pf.printPath(pf.findPlayer());

//console.log("-----------")

//pf2.printPath(pf2.findPlayer(1, 10, 5, 10));








//console.log(getPath(cornerGraph, 2, 2, 7, 17));

//console.log(findPlayer(8.3, 1.1, 6.7, 9.9, cornerGraph, corners));

//console.log(getLine(5, 3, 8, 1));
//console.log(crossesWall([9, 2],[7, 4], field));
},{"node-dijkstra":1}]},{},[6]);
