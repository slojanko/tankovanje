function player(x, y, z, moveSpeed, angleSpeed, startAngle) {
	this.position = [x, y, z];
	
	this.hp = 100;

    this.moveSpeed = moveSpeed;
    this.angleSpeed = angleSpeed;
	this.angle = startAngle;

	this.reload = 0;
	
	this.oldX = 0;
	this.oldY = 0;
}

player.prototype.hit = function(damage){
    if(damage > player.hp){
        damage = player.hp;
	}
	if(- damage + player.hp > 100){
		damage = - (100 - player.hp);
	}

    player.hp = player.hp - damage;
    // calculate the percentage of the total width
    var barWidth = player.hp;
    var hitWidth = damage;
    
    document.getElementById("hit_id").style.width = hitWidth + "%";
    
    setTimeout(function(){
      document.getElementById("hit_id").style.width = "0";
      document.getElementById("bar_id").style.width = barWidth + "%";
    }, 0.5);
}

player.prototype.update = function(pf) {
	if(player.hp == 0){
		return;
	}
	for(var i = 0; i < drops.length; i++){
		if(euclideanDistance(this.position[0], this.position[2], drops[i].position[0], drops[i].position[2]) < 0.5){
			drops[i].pickUp();
		}
	}
	// Handle keyboard
	var inputX = -(input.getKey(vk("A")) || input.getKey(vk_left)) + (input.getKey(vk("D")) || input.getKey(vk_right));
	var inputY = (input.getKey(vk("W")) || input.getKey(vk_up)) - (input.getKey(vk("S")) || input.getKey(vk_down));
	
	if (inputX != 0 || inputY != 0) {
		var target = math.angle(0, 0, inputX, inputY);
		this.angle = this.angle + math.angleDiff(this.angle, target) * this.angleSpeed;

		collision = pf.collides(this.position[2], this.position[0], 0.5, 0.3, this.angle, false)
		if(collision == -1){
			this.oldX = this.position[0];
			this.oldY = this.position[2];
			this.position[0] += this.moveSpeed * Math.cos(math.radians(this.angle));
			this.position[2] -= this.moveSpeed * Math.sin(math.radians(this.angle));
			
		}
		else{
			mov = -1;
			s = 1;
			b = 0.1;
			mov2 = 0.5;
			mov3 = mov2;
			s2 = s;
			b2 = b;
			// Out of map
			if(collision == 10){
				c = -this.angle;
				this.position[0] += mov2 * (s * this.moveSpeed * Math.cos(math.radians(c)));
				this.position[2] += mov2 * (s2 * this.moveSpeed * Math.sin(math.radians(c)));
			}
			else{
					directions = [
						[this.position[0] + mov, this.position[2] + mov],
						[this.position[0], this.position[2] + mov],
						[this.position[0] - mov, this.position[2] + mov],
						[this.position[0] + mov, this.position[2]],
						[this.position[0], this.position[2]],
						[this.position[0] - mov, this.position[2]],
						[this.position[0] + mov, this.position[2] - mov],
						[this.position[0], this.position[2] - mov],
						[this.position[0] - mov, this.position[2] - mov]
						];
	
				a = this.angle;
				angles = [];
				for(var i = 0; i < directions.length; i++){
					angles.push(math.radians(180 + math.angle(this.position[0], this.position[2], directions[i][0], directions[i][1])));
				}
	
				a = pf.getNewAngle(this.position[2], this.position[0], a);
				c = pf.treeCheck(directions[collision][1], directions[collision][0], this.angle, a, collision);
				
				if(c == 179 + this.angle){
	
					b = 0.5;
					b2 = 0.5;
					mov2 = 0.3;
					s = 0.5;
					s2 = 0.5;
				}
				if(a == -1){
					mov2 = 0;
				}
				if(c == -1){
					mov2 = 0;
				}
				if(collision == 4){
					c = 90 + this.angle;
					b = 0;
					b2 = 0;
				}
				
				this.position[0] += mov2 * (s * this.moveSpeed * Math.cos(math.radians(c)) + b * this.moveSpeed * Math.cos(angles[collision]));
				this.position[2] += mov2 * (s2 * this.moveSpeed * Math.sin(math.radians(c)) + b2 * this.moveSpeed * Math.sin(angles[collision]));
			}
		}
	}
}

player.prototype.drawShadow = function(shader) {
	// Body
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(this.angle));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);	
    
	drawModel(shader, playerBodyPositionBuffer, -1, -1, -1);

    // Hatch
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(-math.angle(this.position[0], this.position[2], mouseInteraction[0], mouseInteraction[1])));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);
    
	drawModel(shader, playerHatchPositionBuffer, -1, -1, -1);
}

player.prototype.draw = function(shader) {
	// Body
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(this.angle));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);	
    
	mat4.toInverseMat3(worldMatrix, normalMatrix);
	mat3.transpose(normalMatrix);
    gl.uniformMatrix3fv(shader.normalMatrixUniform, false, normalMatrix);	
    
	drawModel(shader, playerBodyPositionBuffer, playerBodyNormalBuffer, playerBodyTextureBuffer, playerDiffuse);

    // Hatch
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(-math.angle(this.position[0], this.position[2], mouseInteraction[0], mouseInteraction[1])));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);
    
	mat4.toInverseMat3(worldMatrix, normalMatrix);
	mat3.transpose(normalMatrix);
    gl.uniformMatrix3fv(shader.normalMatrixUniform, false, normalMatrix);	
    
	drawModel(shader, playerHatchPositionBuffer, playerHatchNormalBuffer, playerHatchTextureBuffer, playerDiffuse);
}