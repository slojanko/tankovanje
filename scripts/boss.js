function boss(x, y, z, moveSpeed, angleSpeed) {
    this.position = [x, y, z];

    this.path = -1;
    this.up = 31;

    this.moveSpeed = moveSpeed;
    this.angleSpeed = angleSpeed;
    this.angle = 0;
	
	this.time = 0;
    this.startTime = new Date().getTime();

    this.hp = 2000;
    this.reload = 30;

    this.dropProbability = 1.0;

    this.respawn = 2000;
    this.enemySpawnCount = 0;

    sp.maxTanks = 50;

    // For graph
    // this.positionBuffer = gl.createBuffer();
}

boss.prototype.updatePath = function(pf, endX, endZ) {
    this.up = this.up + (71 - Math.sqrt( Math.pow(endX - this.position[0], 2) + Math.pow(endZ - this.position[2], 2) ) ) / (71);
    if (this.path == -1 || this.up > 30 && euclideanDistance(this.position[0], this.position[2], player.position[0], player.position[2]) <= 15){
        this.up = - Math.random() * 2;
        this.path = pf.findPlayer(this.position[2], this.position[0], endZ, endX);
    }
    else if(this.up > 100){
        this.up = - Math.random() * 2;
        this.path = pf.findPlayer(this.position[2], this.position[0], endZ, endX);
    }
}

boss.prototype.hit = function(damage){
    if(damage > this.hp){
        damage = this.hp;
    }

	if(- damage + this.hp > 2000){
		damage = - (2000 - this.hp);
	}

    this.hp = this.hp - damage;
    // calculate the percentage of the total width
    var barWidth = (this.hp / 2000) * 100;
    var hitWidth = (damage / (this.hp + damage)) * 100 + "%";
    
    document.getElementById("hit_id_boss").style.width = hitWidth + "%";
    
    setTimeout(function(){
      document.getElementById("hit_id_boss").style.width = "0";
      document.getElementById("bar_id_boss").style.width = barWidth + "%";
    }, 0.5);
}

boss.prototype.kill = function(index){
    ChickenDefeat.cloneNode(true).play();
    enemies.splice(index, 1);

    probability = Math.random();

    if(probability <= this.dropProbability){
        drops.push(new chickenLeg(this.position[0], this.position[1], this.position[2], this.angle))
    }

    bossDead = true;
}

boss.prototype.update = function() {
    var target = -math.angle(this.position[0], this.position[2], this.path[1][1], this.path[1][0]);
    this.angle = this.angle + math.angleDiff(this.angle, target) * this.angleSpeed;
	
	var timeNow = new Date().getTime();
    var elapsedTime = timeNow - this.startTime;
	
    this.time += elapsedTime;
	this.startTime = timeNow;
	
    this.position[0] += Math.cos(math.radians(this.angle)) * this.moveSpeed;
	this.position[1] = Math.sin(math.radians(this.time/1.6))*0.2 + 0.2;
    this.position[2] -= Math.sin(math.radians(this.angle)) * this.moveSpeed;

    if(euclideanDistance(this.position[0], this.position[2], player.position[0], player.position[2]) <= 0.45){
        if(this.reload <= 0){
            player.hit(15);
            this.reload = 30;
        }
        else{
            this.reload--;
        }
    }
    else{
        this.reload--;
    }

    if(this.hp <= this.respawn && this.hp != 0){
        sp.spawn(this.enemySpawnCount, this.position[2], this.position[0]);
        this.respawn -= 200;
        if(this.enemySpawnCount == 0){
            this.enemySpawnCount = 2;
        }
        else{
            this.enemySpawnCount++;
        }
        sp.chickenProbability -= 0.05;
        sp.chickenTankProbability += 0.05;
        if(sp.chickenProbability < 0.0){
            sp.chickenProbability = 0.0;
        }
        if(sp.chickenTankProbability > 1.0){
            sp.chickenTankProbability = 1.0;
        }
    }
}

function euclideanDistance(x1, y1, x2, y2){
    return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
}

boss.prototype.drawShadow = function(shader) {
    mat4.identity(worldMatrix);
    mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(this.angle));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);	
    
    drawModel(shader, bossPositionBuffer, -1, -1, -1);
}

boss.prototype.draw = function(shader) {
    mat4.identity(worldMatrix);
    mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(this.angle));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);	

    mat4.toInverseMat3(worldMatrix, normalMatrix);
	mat3.transpose(normalMatrix);
    gl.uniformMatrix3fv(shader.normalMatrixUniform, false, normalMatrix);	
    
    drawModel(shader, bossPositionBuffer, bossNormalBuffer, bossTextureBuffer, bossDiffuse);

    // For graph   
    /*
    gl.deleteBuffer(this.positionBuffer);
    this.positionBuffer = gl.createBuffer();

    mat4.identity(worldMatrix);
	gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);
    createGraph(this.path, this.positionBuffer);
    drawGraph(shader, this.positionBuffer);
    //*/
}