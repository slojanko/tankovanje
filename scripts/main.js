// DO NOT RENAME
var canvas;
var gl;
var loaded = 0;
var loadFinish = 28;
var worldMatrix = mat4.create();
var viewMatrix = mat4.create();
var projectionMatrix = mat4.create();
var normalMatrix = mat3.create();

var shadowViewMatrix = mat4.create();
var shadowProjectionMatrix = mat4.create();

var minimapViewMatrix = mat4.create();
var minimapProjectionMatrix = mat4.create();

// Basic shaders
var lightingShader;
var depthShader;

// Texture and framebuffer
var depthFrameBuffer;
var depthTexture;

// Static objects
var wallsPositionBuffer;
var wallsNormalBuffer;
var wallsTextureBuffer;
var wallsDiffuse;

var backgroundPositionBuffer;
var backgroundNormalBuffer;
var backgroundTextureBuffer;
var backgroundDiffuse;

var sprucePositionBuffer;
var spruceNormalBuffer;
var spruceTextureBuffer;
var spruceDiffuse;

var treePositionBuffer;
var treeNormalBuffer;
var treeTextureBuffer;
var treeDiffuse;

var rockPositionBuffer;
var rockNormalBuffer;
var rockTextureBuffer;
var rockDiffuse;

var tentPositionBuffer;
var tentNormalBuffer;
var tentTextureBuffer;
var tentDiffuse;

var nestPositionBuffer;
var nestNormalBuffer;
var nestTextureBuffer;
var nestDiffuse;

// Characters
var playerBodyPositionBuffer;
var playerBodyNormalBuffer;
var playerBodyTextureBuffer;
var playerDiffuse;

var playerHatchPositionBuffer;
var playerHatchNormalBuffer;
var playerHatchTextureBuffer;

var chickenPositionBuffer;
var chickenNormalBuffer;
var chickenTextureBuffer;
var chickenDiffuse;

var chickenTankBodyPositionBuffer;
var chickenTankBodyNormalBuffer;
var chickenTankBodyTextureBuffer;
var chickenTankDiffuse;

var chickenTankHatchPositionBuffer;
var chickenTankHatchNormalBuffer;
var chickenTankHatchTextureBuffer;

var bulletPositionBuffer;
var bulletNormalBuffer;
var bulletTextureBuffer;
var bulletDiffuse;

var chickenLegPositionBuffer;
var chickenLegNormalBuffer;
var chickenLegTextureBuffer;
var chickenLegDiffuse;

var bossPositionBuffer;
var bossNormalBuffer;
var bossTextureBuffer;
var bossDiffuse;

// Spawner
var sp;

// Camera and Player
var player = new player(4, 0, 6, 0.1 * 1, 0.15, 0);
var cameraOffset = [0.0, 10.0, 5.0];
var cameraPosition = math.addv3(player.position, cameraOffset);
var mouseInteraction  = [];

// Enemies
var enemies = [];
var bullets = [];
var drops = [];
var playerBullets = [];
var enemyCount = 0;

var updateInterval;
var clearIv = -1;
var sound3;

var bossDead = false;

// Initialize pathfinding
var field = [
	[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,1],
	[1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,1,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,1],
	[1,0,0,0,0,0,1,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,1],
	[1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,1,0,0,0,0,0,1,1,0,0,0,0,0,1,0,1],
	[1,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,1,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,1,0,0,1,0,1],
	[1,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,1],
	[1,0,0,0,1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,1,1,0,0,0,0,0,1,1,0,1],
	[1,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,1,1,0,0,0,0,0,1,0,0,1],
	[1,0,0,0,1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,1],
	[1,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0,1,0,0,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,1],
	[1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,1],
	[1,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,1],
	[1,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,1,0,0,0,0,0,1,1,0,1,0,0,0,1],
	[1,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,1,1,1,1,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1],
	[1,0,1,0,0,1,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
	[1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,1,0,1],
	[1,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,1,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,1],
	[1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,1,1,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1],
	[1,1,1,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,0,0,1,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,1],
	[1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,1,1,0,0,0,0,0,1],
	[1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,1],
	[1,1,1,1,1,0,0,0,0,0,0,0,0,0,1,0,1,1,1,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,1],
	[1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,1],
	[1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,1,1,0,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,1,1,1,0,0,0,1,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,1,0,0,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,1,0,0,1,0,0,1,1,1,0,0,0,0,0,0,0,1,0,0,0,1],
	[1,0,0,0,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,1,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,1,0,1],
	[1,0,0,0,0,0,0,0,1,1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1],
	[1,0,0,1,0,0,0,0,1,1,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1],
	[1,0,0,0,0,0,0,0,0,1,1,1,0,0,0,1,0,1,0,0,0,0,0,0,0,1,1,1,1,0,0,0,1,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1],
	[1,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1],
	[1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1,0,0,1,1,1],
	[1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,1,1],
	[1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
	[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
];

var pf = new pathfinding();
pf.init(field);
/*
pf.getNewField([[1, 1], [8, 5]])
pf.findPlayer(.......)
*/

function getShader(gl, id) {
	// Pridobi element z id
	var shaderScript = document.getElementById(id);

	if (!shaderScript) {
		return null;
	}

	// Spravi source v string
	var shaderSource = "";
	var currentChild = shaderScript.firstChild;
	while (currentChild) {
	if (currentChild.nodeType == 3) {
			shaderSource += currentChild.textContent;
		}
		currentChild = currentChild.nextSibling;
	}
	
	// Ustvari shader glede na tip elementa
	var shader;
	if (shaderScript.type == "x-shader/x-fragment") {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	} else if (shaderScript.type == "x-shader/x-vertex") {
		shader = gl.createShader(gl.VERTEX_SHADER);
	} else {
		return null;
	}

	// Dodaj source kodo ustvarjenemu shaderju in ga prevedi
	gl.shaderSource(shader, shaderSource);
	gl.compileShader(shader);

	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		alert(gl.getShaderInfoLog(shader));
		return null;
	}

	// Vrni uspesno preveden shader
	return shader;
}

function initShaders() {
	// LIGHTING
	var fragmentShader = getShader(gl, "lighting-fs");
	var vertexShader = getShader(gl, "lighting-vs");

	// Ustvari shader program 
	lightingShader = gl.createProgram();
	gl.attachShader(lightingShader, vertexShader);
	gl.attachShader(lightingShader, fragmentShader);
	gl.linkProgram(lightingShader);

	if (!gl.getProgramParameter(lightingShader, gl.LINK_STATUS)) {
		alert("Unable to initialize the shader program.");
	}

	// Pridobi lokacije atributov in jih vklopi
	lightingShader.PositionAttribute = gl.getAttribLocation(lightingShader, "in_Position");
	gl.enableVertexAttribArray(lightingShader.PositionAttribute);
	
	lightingShader.NormalAttribute = gl.getAttribLocation(lightingShader, "in_Normal");
	gl.enableVertexAttribArray(lightingShader.NormalAttribute);

	lightingShader.TextureAttribute = gl.getAttribLocation(lightingShader, "in_Texture");
	gl.enableVertexAttribArray(lightingShader.TextureAttribute);

	// Shrani lokacijo uniformov
	lightingShader.worldMatrixUniform = gl.getUniformLocation(lightingShader, "u_worldMatrix");
	lightingShader.viewMatrixUniform = gl.getUniformLocation(lightingShader, "u_viewMatrix");
	lightingShader.projectionMatrixUniform = gl.getUniformLocation(lightingShader, "u_projectionMatrix");
	lightingShader.normalMatrixUniform = gl.getUniformLocation(lightingShader, "u_normalMatrix");

	lightingShader.shadowViewUniform = gl.getUniformLocation(lightingShader, "u_shadowView");
	lightingShader.shadowProjectionUniform = gl.getUniformLocation(lightingShader, "u_shadowProjection");
	lightingShader.depthUniform = gl.getUniformLocation(lightingShader, "u_depth");

	lightingShader.ambientColourUniform = gl.getUniformLocation(lightingShader, "u_vAmbientColour");
	lightingShader.lightVectorUniform = gl.getUniformLocation(lightingShader, "u_vLightVector");
	lightingShader.lightColourUniform = gl.getUniformLocation(lightingShader, "u_vLightColour");
	lightingShader.textureUniform = gl.getUniformLocation(lightingShader, "u_2dTexture");

	lightingShader.minimapUniform = gl.getUniformLocation(lightingShader, "u_vMinimap");

	// DEPTH
	fragmentShader = getShader(gl, "depth-fs");
	vertexShader = getShader(gl, "depth-vs");

	// Ustvari shader program 
	depthShader = gl.createProgram();
	gl.attachShader(depthShader, vertexShader);
	gl.attachShader(depthShader, fragmentShader);
	gl.linkProgram(depthShader);

	if (!gl.getProgramParameter(depthShader, gl.LINK_STATUS)) {
		alert("Unable to initialize the shader program.");
	}

	// Pridobi lokacije atributov in jih vklopi
	depthShader.PositionAttribute = gl.getAttribLocation(depthShader, "in_Position");
	gl.enableVertexAttribArray(depthShader.PositionAttribute);

	// Shrani lokacijo uniformov
	depthShader.worldMatrixUniform = gl.getUniformLocation(depthShader, "u_worldMatrix");
	depthShader.viewMatrixUniform = gl.getUniformLocation(depthShader, "u_viewMatrix");
	depthShader.projectionMatrixUniform = gl.getUniformLocation(depthShader, "u_projectionMatrix");
}

function initTextureFramebuffer() {
	// Set depth texture and buffer
	depthFrameBuffer = gl.createFramebuffer();
	gl.bindFramebuffer(gl.FRAMEBUFFER, depthFrameBuffer);
	depthFrameBuffer.width = 2048;
	depthFrameBuffer.height = 2048;
  
	depthTexture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, depthTexture);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, depthFrameBuffer.width, depthFrameBuffer.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
  
	var renderbuffer = gl.createRenderbuffer();
	gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
	gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, depthFrameBuffer.width, depthFrameBuffer.height);
  
	gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, depthTexture, 0);
	gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);
  
	gl.bindTexture(gl.TEXTURE_2D, null);
	gl.bindRenderbuffer(gl.RENDERBUFFER, null);
	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
}

function handleTextureLoaded(texture) {
	gl.bindTexture(gl.TEXTURE_2D, texture);
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR); // Kaj se z njo dogaja, ce jo povecujemo
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR); // Kaj se z njo dogaja, ce jo zmanjsujemo gl.LINEAR_MIPMAP_NEAREST
	gl.generateMipmap(gl.TEXTURE_2D);
	gl.bindTexture(gl.TEXTURE_2D, null);
}

function initTextures() {
	// Load character textures
	playerDiffuse = gl.createTexture();
	playerDiffuse.image = new Image();
	playerDiffuse.image.onload = function() {
		handleTextureLoaded(playerDiffuse);
		loaded+=1;
	};
	playerDiffuse.image.src = "textures/player_diffuse.png";

	chickenDiffuse = gl.createTexture();
	chickenDiffuse.image = new Image();
	chickenDiffuse.image.onload = function() {
		handleTextureLoaded(chickenDiffuse);
		loaded+=1;
	};
	chickenDiffuse.image.src = "textures/chicken_diffuse.png";

	chickenTankDiffuse = gl.createTexture();
	chickenTankDiffuse.image = new Image();
	chickenTankDiffuse.image.onload = function() {
		handleTextureLoaded(chickenTankDiffuse);
		loaded+=1;
	};
	chickenTankDiffuse.image.src = "textures/chicken_tank_diffuse.png";

	bulletDiffuse = gl.createTexture();
	bulletDiffuse.image = new Image();
	bulletDiffuse.image.onload = function() {
		handleTextureLoaded(bulletDiffuse);
		loaded+=1;
	};
	bulletDiffuse.image.src = "textures/chick_diffuse.png";

	// Load static object textures
	wallsDiffuse = gl.createTexture();
	wallsDiffuse.image = new Image();
	wallsDiffuse.image.onload = function() {
		handleTextureLoaded(wallsDiffuse);
		loaded+=1;
	};
	wallsDiffuse.image.src = "textures/walls_diffuse.png";

	backgroundDiffuse = gl.createTexture();
	backgroundDiffuse.image = new Image();
	backgroundDiffuse.image.onload = function() {
		handleTextureLoaded(backgroundDiffuse);
		loaded+=1;
	};
	backgroundDiffuse.image.src = "textures/background_diffuse.png";

	spruceDiffuse = gl.createTexture();
	spruceDiffuse.image = new Image();
	spruceDiffuse.image.onload = function() {
		handleTextureLoaded(spruceDiffuse);
		loaded+=1;
	};
	spruceDiffuse.image.src = "textures/spruce_diffuse.png";

	treeDiffuse = gl.createTexture();
	treeDiffuse.image = new Image();
	treeDiffuse.image.onload = function() {
		handleTextureLoaded(treeDiffuse);
		loaded+=1;
	};
	treeDiffuse.image.src = "textures/tree_diffuse.png";

	rockDiffuse = gl.createTexture();
	rockDiffuse.image = new Image();
	rockDiffuse.image.onload = function() {
		handleTextureLoaded(rockDiffuse);
		loaded+=1;
	};
	rockDiffuse.image.src = "textures/rock_diffuse.png";

	tentDiffuse = gl.createTexture();
	tentDiffuse.image = new Image();
	tentDiffuse.image.onload = function() {
		handleTextureLoaded(tentDiffuse);
		loaded+=1;
	};
	tentDiffuse.image.src = "textures/tent_diffuse.png";

	nestDiffuse = gl.createTexture();
	nestDiffuse.image = new Image();
	nestDiffuse.image.onload = function() {
		handleTextureLoaded(nestDiffuse);
		loaded+=1;
	};
	nestDiffuse.image.src = "textures/nest_diffuse.png";

	chickenLegDiffuse = gl.createTexture();
	chickenLegDiffuse.image = new Image();
	chickenLegDiffuse.image.onload = function() {
		handleTextureLoaded(chickenLegDiffuse);
		loaded+=1;
	};
	chickenLegDiffuse.image.src = "textures/chicken_leg_diffuse.png";

	bossDiffuse = gl.createTexture();
	bossDiffuse.image = new Image();
	bossDiffuse.image.onload = function() {
		handleTextureLoaded(bossDiffuse);
		loaded+=1;
	};
	bossDiffuse.image.src = "textures/boss_diffuse.png";
}

function loadModel(path, positionBuffer, normalBuffer, textureBuffer) {
	var request = new XMLHttpRequest();
	request.open("GET", path);
	request.onreadystatechange = function () 
	{
		if (request.readyState == 4) 
		{
			var position = [];
			var normal = [];
			var texture = [];
			var index = [];
			var lines = request.responseText.split("\n");
			for(var i = 0; i < lines.length; i++)
			{
				var line = lines[i].split(" ");
				if (line[0] == "v")
					position.push(parseFloat(line[1]), parseFloat(line[2]), parseFloat(line[3]));
				else if (line[0] == "vn")
					normal.push(parseFloat(line[1]), parseFloat(line[2]), parseFloat(line[3]));
				else if (line[0] == "vt")
					texture.push(parseFloat(line[1]), parseFloat(line[2]));
				else if (line[0] == "f")
					for(var j = 1; j < 4; j++)
					{
						var lineIndex = line[j].split("/");
						index.push(parseFloat(lineIndex[0]), parseFloat(lineIndex[1]), parseFloat(lineIndex[2]));
					}
			}
			createModel(position, normal, texture, index, positionBuffer, normalBuffer, textureBuffer);

			loaded+=1;
		}
	}
	
	request.send();
}

function createModel(position, normal, texture, index, positionBuffer, normalBuffer, textureBuffer) {
	var positionData = [];
	var normalData = [];
	var textureData = [];

	var len = index.length;
	for(var i = 0; i < len; i+=3)
	{
		var pos = (index[i] - 1) * 3;
		var tex = (index[i+1] - 1) * 2;
		var nor = (index[i+2] - 1)  * 3;
		positionData.push(position[pos], position[pos+1], position[pos+2]);
		textureData.push(texture[tex], texture[tex+1]);
		normalData.push(normal[nor], normal[nor+1], normal[nor+2]);
	}

	// Zacni spreminjati izbrani buffer
	gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positionData), gl.STATIC_DRAW);
	positionBuffer.itemSize = 3;
	positionBuffer.numItems = positionData.length / 3;

	// Zacni spreminjati izbrani buffer
	gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);

	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normalData), gl.STATIC_DRAW);
	normalBuffer.itemSize = 3;
	normalBuffer.numItems = normalData.length / 3;

	// Zacni spreminjati izbrani buffer
	gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);

	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureData), gl.STATIC_DRAW);
	textureBuffer.itemSize = 2;
	textureBuffer.numItems = textureData.length / 2;
}

// Pass -1 for last 3 arguments if they're not defined by the shaders
function drawModel(shader, positionBuffer, normalBuffer, textureBuffer, diffuse) {
	gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
	gl.vertexAttribPointer(shader.PositionAttribute, positionBuffer.itemSize, gl.FLOAT, false, 0, 0);

	if (normalBuffer != -1)
	{
		gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
		gl.vertexAttribPointer(shader.NormalAttribute, normalBuffer.itemSize, gl.FLOAT, false, 0, 0);
	}
	if (textureBuffer != -1) 
	{
		gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);
		gl.vertexAttribPointer(shader.TextureAttribute, textureBuffer.itemSize, gl.FLOAT, false, 0, 0);
	}

	if (diffuse != -1)
	{
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, diffuse);
		gl.uniform1i(shader.textureUniform, 0);
	}

	gl.drawArrays(gl.TRIANGLES, 0, positionBuffer.numItems);
}

function initBuffers() {
	// Load static objects
	wallsPositionBuffer = gl.createBuffer();
	wallsNormalBuffer = gl.createBuffer();
	wallsTextureBuffer = gl.createBuffer();
	loadModel("models/walls.obj", wallsPositionBuffer, wallsNormalBuffer, wallsTextureBuffer);

	backgroundPositionBuffer = gl.createBuffer();
	backgroundNormalBuffer = gl.createBuffer();
	backgroundTextureBuffer = gl.createBuffer();
	loadModel("models/background.obj", backgroundPositionBuffer, backgroundNormalBuffer, backgroundTextureBuffer);

	sprucePositionBuffer = gl.createBuffer();
	spruceNormalBuffer = gl.createBuffer();
	spruceTextureBuffer = gl.createBuffer();
	loadModel("models/spruce.obj", sprucePositionBuffer, spruceNormalBuffer, spruceTextureBuffer);

	treePositionBuffer = gl.createBuffer();
	treeNormalBuffer = gl.createBuffer();
	treeTextureBuffer = gl.createBuffer();
	loadModel("models/tree.obj", treePositionBuffer, treeNormalBuffer, treeTextureBuffer);

	rockPositionBuffer = gl.createBuffer();
	rockNormalBuffer = gl.createBuffer();
	rockTextureBuffer = gl.createBuffer();
	loadModel("models/rock.obj", rockPositionBuffer, rockNormalBuffer, rockTextureBuffer);

	tentPositionBuffer = gl.createBuffer();
	tentNormalBuffer = gl.createBuffer();
	tentTextureBuffer = gl.createBuffer();
	loadModel("models/tent.obj", tentPositionBuffer, tentNormalBuffer, tentTextureBuffer);

	nestPositionBuffer = gl.createBuffer();
	nestNormalBuffer = gl.createBuffer();
	nestTextureBuffer = gl.createBuffer();
	loadModel("models/nest.obj", nestPositionBuffer, nestNormalBuffer, nestTextureBuffer);

	// Load characters
	playerBodyPositionBuffer = gl.createBuffer();
	playerBodyNormalBuffer = gl.createBuffer();
	playerBodyTextureBuffer = gl.createBuffer();
	loadModel("models/player_body.obj", playerBodyPositionBuffer, playerBodyNormalBuffer, playerBodyTextureBuffer);

	playerHatchPositionBuffer = gl.createBuffer();
	playerHatchNormalBuffer = gl.createBuffer();
	playerHatchTextureBuffer = gl.createBuffer();
	loadModel("models/player_hatch.obj", playerHatchPositionBuffer, playerHatchNormalBuffer, playerHatchTextureBuffer);

	chickenPositionBuffer = gl.createBuffer();
	chickenNormalBuffer = gl.createBuffer();
	chickenTextureBuffer = gl.createBuffer();
	loadModel("models/chicken.obj", chickenPositionBuffer, chickenNormalBuffer, chickenTextureBuffer);

	chickenTankBodyPositionBuffer = gl.createBuffer();
	chickenTankBodyNormalBuffer = gl.createBuffer();
	chickenTankBodyTextureBuffer = gl.createBuffer();
	loadModel("models/chicken_tank_body.obj", chickenTankBodyPositionBuffer, chickenTankBodyNormalBuffer, chickenTankBodyTextureBuffer);

	chickenTankHatchPositionBuffer = gl.createBuffer();
	chickenTankHatchNormalBuffer = gl.createBuffer();
	chickenTankHatchTextureBuffer = gl.createBuffer();
	loadModel("models/chicken_tank_hatch.obj", chickenTankHatchPositionBuffer, chickenTankHatchNormalBuffer, chickenTankHatchTextureBuffer);

	bulletPositionBuffer = gl.createBuffer();
	bulletNormalBuffer = gl.createBuffer();
	bulletTextureBuffer = gl.createBuffer();
	loadModel("models/chick.obj", bulletPositionBuffer, bulletNormalBuffer, bulletTextureBuffer);

	chickenLegPositionBuffer = gl.createBuffer();
	chickenLegNormalBuffer = gl.createBuffer();
	chickenLegTextureBuffer = gl.createBuffer();
	loadModel("models/chicken_leg.obj", chickenLegPositionBuffer, chickenLegNormalBuffer, chickenLegTextureBuffer);

	bossPositionBuffer = gl.createBuffer();
	bossNormalBuffer = gl.createBuffer();
	bossTextureBuffer = gl.createBuffer();
	loadModel("models/boss.obj", bossPositionBuffer, bossNormalBuffer, bossTextureBuffer);
}

function createGraph(path, positionBuffer) {
	var positionArray = [];
	for(var i = 0; i < path.length; i++) {
		positionArray.push(path[i][1], 0.1, path[i][0]);
	}	
	gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positionArray), gl.STATIC_DRAW);
	positionBuffer.itemSize = 3;
	positionBuffer.numItems = path.length
}

function drawGraph(shader, positionBuffer) {
	gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
	gl.vertexAttribPointer(shader.PositionAttribute, positionBuffer.itemSize, gl.FLOAT, false, 0, 0);

	gl.drawArrays(gl.LINE_STRIP, 0, positionBuffer.numItems);
}

function initAudio() {
	Sounds = new Audio("Shoot2.m4a");
	Sounds.volume = 1;
	Sounds.load();

	ChickenDefeat = new Audio("ChickenScream.m4a");
	ChickenDefeat.volume = 1;
	ChickenDefeat.load();

	Background = new Audio("Background.mp3");
	Background.volume = 1;
	Background.load();
	Background.play();
}

// Original implementacija mouse plane collision
/*
var _x = 1280/720 * dtan(60 / 2) * (2 * window_mouse_get_x() / window_get_width() - 1)
var _y = dtan(60 / 2) * (2 * window_mouse_get_y() / window_get_height() - 1);
mouse_vector_x = _x * cameraLookAt[0] + _y * cameraLookAt[1] + cameraLookAt[2]
mouse_vector_y = _x * cameraLookAt[4] + _y * cameraLookAt[5] + cameraLookAt[6]
mouse_vector_z = _x * cameraLookAt[8] + _y * cameraLookAt[9] + cameraLookAt[10]

// Mouse vector isn't parallel to floor
if (mouse_vector_z != 0) 
{  
   var _t = -camera.offsetZ / mouse_vector_z;
   
   // Mouse vector is not pointing away from floor
   if (_t >= 0) 
   {  
       mouse3dX = camera.cameraX + mouse_vector_x * _t;
       mouse3dY = camera.cameraY + mouse_vector_y * _t;
   }
}
*/

function mouseIntersection(fov) {
	var _x = gl.aspectRatio * Math.tan(math.radians(fov / 2)) * (1 - 2 * input.mouseX / gl.viewportWidth)
	var _y = Math.tan(math.radians(fov/2)) * (2 * input.mouseY / gl.viewportHeight - 1);

	var mouse_vector_x = _x * viewMatrix[0] + _y * viewMatrix[1] + viewMatrix[2];
	var mouse_vector_y = _x * viewMatrix[8] + _y * viewMatrix[9] + viewMatrix[10];
	var mouse_vector_z = _x * viewMatrix[4] + _y * viewMatrix[5] + viewMatrix[6];

	// Mouse vector isn't parallel to floor
	if (mouse_vector_z != 0) 
	{  
		var _t = -cameraPosition[1] / mouse_vector_z;
	
		// Mouse vector is not pointing away from floor
		if (_t <= 0) 
		{  
			var mouse3dX = cameraPosition[0] + mouse_vector_x * _t;
			var mouse3dZ = cameraPosition[2] + mouse_vector_y * _t;

			return [mouse3dX, mouse3dZ];
		}
	}

	// Else return previous value
	return mouseInteraction;
}

function initGL(canvas) 
{
	gl = null;

	try {
		// Pridobi context 
		gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
		gl.viewportWidth = canvas.width;
		gl.viewportHeight = canvas.height;
		gl.aspectRatio = canvas.width / canvas.height;
	}
	catch(e) {}

	if (!gl) {
		alert("Brskalnik ne podpira WebGL.");
	}
	
	return gl;
}

function start() {
	document.getElementById("tipka").style.display = "none";
	document.getElementById("startpicture").style.display = "none";

	canvas = document.getElementById("glcanvas");
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
	canvas.style.zIndex = "0";
	canvas.style.position = "absolute";

	gl = initGL(canvas);

	if (gl) 
	{
		// Set clear colour and depth
		gl.clearColor(51/255, 153/255, 255/255, 1.0);
		gl.clearDepth(1.0);
		gl.enable(gl.DEPTH_TEST);
		gl.depthFunc(gl.LEQUAL);
		
		// Set viewport
		gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);

		// Initialize components
		initShaders();
		initBuffers();
		initTextures();
		initTextureFramebuffer();
		initAudio();
		
		// Build perspective and shadow matrices
		mat4.perspective(45, gl.aspectRatio, 0.1, 100.0, projectionMatrix);
		mat4.ortho(-17, 17, -17, 17, 0.1, 250, shadowProjectionMatrix);
		mat4.ortho(-12, 12, -12, 12, 0.1, 20, minimapProjectionMatrix);

		// Spawn enemies
		numberOfEnemies = 10;
		maxTanks = 3;
		chickenProb = 0.77;
		chickenTankProb = 0.23;

		sp = new spawner(chickenProb, chickenTankProb, maxTanks);
		sp.spawn(numberOfEnemies, -1, -1);

		// Bind key events to functions
		document.onkeydown = handleKeyDown;
		document.onkeyup = handleKeyUp;
		canvas.onmousedown = handleMouseDown;
		document.onmouseup = handleMouseUp;
		canvas.onmousemove = handleMouseMove;

		updateInterval = setInterval(function() {
			if (loaded == loadFinish) {
				if (document.getElementById("hp").style.display != "unset"){
					var hpBar = document.getElementById("hp");

					hpBar.style.display = "unset";

					hpBarY = (window.innerWidth/2 - 50) + "px";
					hpBarX = window.innerHeight/2 + 30 + "px";
					hpBar.style.margin = hpBarX + " " + hpBarY;
				}
					
				update(); 
				draw();
				updateInput();
			}
		}, 1000/30);
	}
}

function update() {	
	// Handle mouse collision with ground
	mouseInteraction = mouseIntersection(45);

	if(input.getMouse() && player.reload <= 0){
		playerBullets.push(new playerBullet(player.position[0] , player.position[1] + 0.37, player.position[2], 0.15, -math.angle(player.position[0], player.position[2], mouseInteraction[0], mouseInteraction[1])));
		player.reload = 20;
	}
	player.reload--;
	
	// Update player
	player.update(pf);

	// Update chickens
	for (var i in enemies) {
		enemies[i].updatePath(pf, player.position[0], player.position[2]);
		enemies[i].update();
	}

	// Update bullets
	for (var i in bullets) {
		bullets[i].update();
	}
	for (var i in playerBullets) {
		playerBullets[i].update();
	}

	// Update camera
	cameraPosition = math.lerpv3(cameraPosition, math.addv3(player.position, cameraOffset), 0.1);
	mat4.lookAt(cameraPosition, player.position, [0.0, 1.0, 0.0], viewMatrix);
	mat4.lookAt(math.addv3(player.position, [100.0, 100.0, 100.0]), player.position, [0.0, 1.0, 0.0], shadowViewMatrix);
	mat4.lookAt(math.addv3(player.position, [0.0, 10.0, 0.001]), player.position, [0.0, 1.0, 0.0], minimapViewMatrix);

	// Set enemy positions to update pathfinding for current frame
	/*
	var enemyPositions = [];
	for (var i in chickens) {
		enemyPositions.push([chickens[i].z, chickens[i].x]);
	}
	pf.getNewField(enemyPositions);
	*/

	if(enemies.length == 0 && !bossDead){
		sp.spawnBoss();

		// Show boss hp bar
		var bossBar = document.getElementById("hpBoss");

		bossBar.style.display = "unset";
		bossBarW = 7/10 * window.innerWidth;
		bossBar.style.width = bossBarW + "px";
		bossBarX = 19*canvas.height/20 + "px";
		bossBarY = (window.innerWidth/2 - (bossBarW/2)) + "px";
					
		bossBar.style.margin = bossBarX + " " + bossBarY;
	}
	if(enemies.length == 0 && bossDead){
		document.getElementById("lost").innerHTML="You won! You killed all the chickens!";
		if(clearIv == -1){
			clearIv = 20;
		}
		clearIv--;
	}
	if(player.hp == 0){
		if(clearIv == -1){
			clearIv = 20;
		}
		clearIv--;
	}
	if(clearIv == 0){
		clearInterval(updateInterval);
		canvas.style.display = "none";
		document.getElementById("hp").style.display = "none";
		document.getElementById("lost").style.display = "block";
	}
}

function drawShadows() {
	// Attributes are global, meaning #1 and #2 are also active when switching to a different shader, we don't need those so disable them
	// gl.disableVertexAttribArray(1);
	// gl.disableVertexAttribArray(2);

	gl.bindFramebuffer(gl.FRAMEBUFFER, depthFrameBuffer);
	gl.viewport(0, 0, depthFrameBuffer.width, depthFrameBuffer.height);

	// Clear previous image
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(depthShader);

	// Set projection and view matrix
	gl.uniformMatrix4fv(depthShader.viewMatrixUniform, false, shadowViewMatrix);
	gl.uniformMatrix4fv(depthShader.projectionMatrixUniform, false, shadowProjectionMatrix);

	// Set static drawing values
	mat4.identity(worldMatrix);
	gl.uniformMatrix4fv(depthShader.worldMatrixUniform, false, worldMatrix);	

	// Start drawing static objects
	drawModel(depthShader, wallsPositionBuffer, -1, -1, -1);
	drawModel(depthShader, backgroundPositionBuffer, -1, -1, -1);
	drawModel(depthShader, sprucePositionBuffer, -1, -1, -1);
	drawModel(depthShader, treePositionBuffer, -1, -1, -1);
	drawModel(depthShader, rockPositionBuffer, -1, -1, -1);
	drawModel(depthShader, tentPositionBuffer, -1, -1, -1);
	drawModel(depthShader, nestPositionBuffer, -1, -1, -1);

	// Draw player
	player.drawShadow(depthShader);

	// Draw chickens
	for (var i in enemies) {
		enemies[i].drawShadow(depthShader);
	}

	// Draw bullets
	for (var i in bullets) {
		bullets[i].drawShadow(depthShader);
	}
	for (var i in playerBullets) {
		playerBullets[i].drawShadow(depthShader);
	}

	// Draw drops
	for (var i in drops) {
		drops[i].drawShadow(depthShader);
	}

	// Reenable attributes
	// gl.enableVertexAttribArray(lightingShader.NormalAttribute);
	// gl.enableVertexAttribArray(lightingShader.TextureAttribute);

	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
}

function drawScene()
{
	// Set projection and view matrix
	gl.uniformMatrix4fv(lightingShader.shadowViewUniform, false, shadowViewMatrix);
	gl.uniformMatrix4fv(lightingShader.shadowProjectionUniform, false, shadowProjectionMatrix);

	// Set lighting information
	gl.uniform3f(lightingShader.ambientColourUniform, 0.3, 0.3, 0.3);
	gl.uniform3f(lightingShader.lightVectorUniform, -1.0, -1.0, -1.0);
	gl.uniform3f(lightingShader.lightColourUniform, 1.0, 0.9, 0.75); // Light orange

	// Set static drawing values
	mat4.identity(worldMatrix);
	gl.uniformMatrix4fv(lightingShader.worldMatrixUniform, false, worldMatrix);	
	mat3.identity(normalMatrix);
	gl.uniformMatrix3fv(lightingShader.normalMatrixUniform, false, normalMatrix);	

	// Start drawing static objects
	drawModel(lightingShader, wallsPositionBuffer, wallsNormalBuffer, wallsTextureBuffer, wallsDiffuse);
	drawModel(lightingShader, backgroundPositionBuffer, backgroundNormalBuffer, backgroundTextureBuffer, backgroundDiffuse);
	drawModel(lightingShader, sprucePositionBuffer, spruceNormalBuffer, spruceTextureBuffer, spruceDiffuse);
	drawModel(lightingShader, treePositionBuffer, treeNormalBuffer, treeTextureBuffer, treeDiffuse);
	drawModel(lightingShader, rockPositionBuffer, rockNormalBuffer, rockTextureBuffer, rockDiffuse);
	drawModel(lightingShader, tentPositionBuffer, tentNormalBuffer, tentTextureBuffer, tentDiffuse);
	drawModel(lightingShader, nestPositionBuffer, nestNormalBuffer, nestTextureBuffer, nestDiffuse);

	// Draw player
	player.draw(lightingShader);

	// Draw chickens
	for (var i in enemies) {
		enemies[i].draw(lightingShader);
	}

	// Draw bullets
	for (var i in bullets) {
		bullets[i].draw(lightingShader);
	}
	for (var i in playerBullets) {
		playerBullets[i].draw(lightingShader);
	}

	// Draw drops
	for (var i in drops) {
		drops[i].draw(lightingShader);
	}
}

function draw() {
	drawShadows();
	
	// Clear previous image
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(lightingShader);
	gl.activeTexture(gl.TEXTURE1);
	gl.bindTexture(gl.TEXTURE_2D, depthTexture);
	gl.uniform1i(lightingShader.depthUniform, 1);

	// Set projection and view matrix
	gl.uniformMatrix4fv(lightingShader.viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(lightingShader.projectionMatrixUniform, false, projectionMatrix);
	gl.uniform3f(lightingShader.minimapUniform, 0.0, 0.0, 0.0);
	drawScene();

	// Draw minimap
	// Set projection and view matrix
	gl.uniformMatrix4fv(lightingShader.viewMatrixUniform, false, minimapViewMatrix);
	gl.uniformMatrix4fv(lightingShader.projectionMatrixUniform, false, minimapProjectionMatrix);
	gl.viewport(0, 0, gl.viewportWidth / 7, gl.viewportWidth / 7);
	gl.uniform3f(lightingShader.minimapUniform, gl.viewportWidth / 7, gl.viewportWidth / 7, 1.0);
	drawScene();
}