function bullet(x, y, z, moveSpeed, startAngle) {
    this.position = [x, y, z];

    this.path = -1;

    this.moveSpeed = moveSpeed;
    this.angle = startAngle;

    this.check = 0;

    this.index = bullets.length;

    //Sounds.cloneNode(true).play();
}

bullet.prototype.changeIndex = function(index){
    this.index = index;
}

bullet.prototype.update = function() {
    this.position[0] += Math.cos(math.radians(this.angle)) * this.moveSpeed;
    this.position[2] -= Math.sin(math.radians(this.angle)) * this.moveSpeed;
    if(this.check == 0){
        if(euclideanDistance(this.position[0], this.position[2], player.position[0], player.position[2]) <= 0.5){
            deleteBullet(this.index);
            player.hit(25);
        }
    
        if(pf.collides(this.position[2], this.position[0], 0.5, 0.5, this.angle, true) != -1){
            deleteBullet(this.index);
        }
        this.check = 3;
    }
    this.check--;
}

function deleteBullet(index){
    bullets.splice(index, 1);

    for(var i = 0; i < bullets.length; i++){
        bullets[i].changeIndex(i);
    }
}

function euclideanDistance(x1, y1, x2, y2){
    return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
}

bullet.prototype.drawShadow = function(shader) {
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(this.angle));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);	
    
	drawModel(shader, bulletPositionBuffer, -1, -1, -1);
}

bullet.prototype.draw = function(shader) {
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(this.angle));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);	
    
	mat4.toInverseMat3(worldMatrix, normalMatrix);
	mat3.transpose(normalMatrix);
    gl.uniformMatrix3fv(shader.normalMatrixUniform, false, normalMatrix);	
    
	drawModel(shader, bulletPositionBuffer, bulletNormalBuffer, bulletTextureBuffer, bulletDiffuse);
}