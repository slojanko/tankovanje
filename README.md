## Tankovanje
**Načrt**

1. ~~Implementiraj graph pathfinding, ki deluje po principu move along shortest path or to player~~
2. Posodobi graph pathfinding:
    - sovražniki se med sabo ne zaletavajo
	- na določenih mestih nepravilno najde pot v Z smeri
3. Posodobi import modelov, da podpira texture coordinates
4. Organiziraj funkcije v objekte, shranjeni v posameznih datotekah