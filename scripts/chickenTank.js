function chickenTank(x, y, z, moveSpeed, angleSpeed) {
    this.position = [x, y, z];

    this.path = -1;
    this.up = 31;

    this.hp = 500;

    this.dropProbability = 0.7;

    this.reload = 0;
    this.moveSpeed = moveSpeed;
    this.angleSpeed = angleSpeed;
    this.angle = 0;
}

chickenTank.prototype.updatePath = function(pf, endX, endZ) {
    this.up = this.up + (71 - Math.sqrt( Math.pow(endX - this.position[0], 2) + Math.pow(endZ - this.position[2], 2) ) ) / (71);
    if (this.up > 30){
        this.up = 0;
        this.path = pf.findPlayer(this.position[2], this.position[0], endZ, endX);
    }
}

chickenTank.prototype.kill = function(index){
    ChickenDefeat.cloneNode(true).play();
    enemies.splice(index, 1);

    probability = Math.random();

    if(probability <= this.dropProbability){
        drops.push(new chickenLeg(this.position[0], this.position[1], this.position[2], this.angle))
    }

    tankCount--;
}

chickenTank.prototype.update = function() {
    var target = -math.angle(this.position[0], this.position[2], this.path[1][1], this.path[1][0]);
    this.angle = this.angle + math.angleDiff(this.angle, target) * this.angleSpeed;

    this.position[0] += Math.cos(math.radians(this.angle)) * this.moveSpeed;
    this.position[2] -= Math.sin(math.radians(this.angle)) * this.moveSpeed;
    
    if(this.reload == 0){
        if(euclideanDistance(this.position[0], this.position[2], player.position[0], player.position[2]) < 15 && pf.crossesWall(this.position[2], this.position[0], player.position[2], player.position[0]) == false){
            bullets.push(new bullet(this.position[0], this.position[1] + 0.37, this.position[2], 0.15, -math.angle(this.position[0], this.position[2], player.position[0], player.position[2])));
        }
        
        this.reload = 70;
    }
    else{
        this.reload--;
    }
}

chickenTank.prototype.hit = function(damage){
    if(damage > this.hp){
        damage = this.hp;
    }
    
    this.hp = this.hp - damage;
}

function euclideanDistance(x1, y1, x2, y2){
    return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
}

chickenTank.prototype.drawShadow = function(shader) {
    // Body
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(this.angle));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);	
    
	drawModel(shader, chickenTankBodyPositionBuffer, -1, -1, -1);

    // Hatch
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(-math.angle(this.position[0], this.position[2], player.position[0], player.position[2])));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);
    
	drawModel(shader, chickenTankHatchPositionBuffer, -1, -1, -1);
}

chickenTank.prototype.draw = function(shader) {
    // Body
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(this.angle));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);	
    
	mat4.toInverseMat3(worldMatrix, normalMatrix);
	mat3.transpose(normalMatrix);
    gl.uniformMatrix3fv(shader.normalMatrixUniform, false, normalMatrix);	
    
	drawModel(shader, chickenTankBodyPositionBuffer, chickenTankBodyNormalBuffer, chickenTankBodyTextureBuffer, chickenTankDiffuse);

    // Hatch
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(-math.angle(this.position[0], this.position[2], player.position[0], player.position[2])));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);
    
	mat4.toInverseMat3(worldMatrix, normalMatrix);
	mat3.transpose(normalMatrix);
    gl.uniformMatrix3fv(shader.normalMatrixUniform, false, normalMatrix);	
    
	drawModel(shader, chickenTankHatchPositionBuffer, chickenTankHatchNormalBuffer, chickenTankHatchTextureBuffer, chickenTankDiffuse);
}