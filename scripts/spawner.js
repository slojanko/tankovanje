var chicken_type = 1;
var tank_type = 2;
var tankCount = 0;

function spawner(chickenProbability, chickenTankProbability, maxTanks){
	this.chickenProbability = chickenProbability;
	this.chickenTankProbability = chickenTankProbability;
	this.maxTanks = maxTanks;
}

spawner.prototype.spawn = function(num, x, y) {
	var count = 0;

	var spawnData = [];

	while(count < num){
		if(tankCount < this.maxTanks){
			if(Math.random() <= this.chickenProbability){
				if(x == -1 || y == -1){
					spawnData.push(spawnOne(chicken_type));
				}
				else{
					spawnData.push([x, y, chicken_type]);
				}
			}
			else{
				if(x == -1 || y == -1){
					spawnData.push(spawnOne(tank_type));
				}
				else{
					spawnData.push([x, y, tank_type]);
				}
				tankCount++;
			}
		}
		else{
			if(x == -1 || y == -1){
				spawnData.push(spawnOne(chicken_type));
			}
			else{
				spawnData.push([x, y, chicken_type]);
			}
		}

		count++;
	}

	for(var i = 0; i < num; i++){
		if(spawnData[i][2] == 1){
			enemies.push(new chicken(spawnData[i][1], 0, spawnData[i][0], 0.05 * (1 + Math.random() * 0.5), 0.1));
		}
		else{
			enemies.push(new chickenTank(spawnData[i][1], 0, spawnData[i][0], 0.05 * (1 + Math.random() * 0.5), 0.1));
		}
	}
}

spawner.prototype.spawnBoss = function() {
		x = 20;
		z = 20;
		enemies.push(new boss(x, 0, z, 0.03 * (1 + Math.random() * 0.5), 0.1));
}
function spawnOne(type){
	x = Math.floor(Math.random() * 51);
	y = Math.floor(Math.random() * 51);

	if(pf.check(x, y)){
		return [x, y, type];
	}
	else{
		return spawnOne(type);
	}
}