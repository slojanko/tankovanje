// Create object that hold functions
math = {};

math.lerpf = function(a,b,t) {
	return a + (b-a) * t;
}

math.lerpv2 = function(a,b,t) {
	return [math.lerpf(a[0], b[0], t), math.lerpf(a[1], b[1], t)];
}

math.lerpv3 = function(a,b,t) {
	return [math.lerpf(a[0], b[0], t), math.lerpf(a[1], b[1], t), math.lerpf(a[2], b[2], t)];
}

math.addv3 = function(a,b) {
	return [a[0] + b[0], a[1] + b[1], a[2] + b[2]];
}

math.angleDiff = function(s, t) {
	a = t-s;
	return (math.modProper(a + 180, 360) - 180);
}

math.modProper = function(n, m) {
	return n - Math.floor(n/m) * m;
}

math.angle = function(x1, y1, x2, y2) {
	return math.degrees(Math.atan2(y2 - y1, x2 - x1));
}

math.radians = function(d) {
	return d * Math.PI / 180;
}

math.degrees = function(r) {
	return r * 180 / Math.PI;
}

math.pointDistance = function(x1, y1, x2, y2) {
	var x = x1 - x2;
	var y = y1 - y2;
	return Math.sqrt(x*x + y*y);
}