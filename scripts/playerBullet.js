function playerBullet(x, y, z, moveSpeed, startAngle) {
    this.position = [x, y, z];

    this.path = -1;

    this.check = 0;

    this.moveSpeed = moveSpeed;
    this.angle = startAngle;

    this.index = playerBullets.length;

    //Sounds.cloneNode(true).play();
}

playerBullet.prototype.changeIndex = function(index){
    this.index = index;
}

playerBullet.prototype.update = function() {
    this.position[0] += Math.cos(math.radians(this.angle)) * this.moveSpeed;
    this.position[2] -= Math.sin(math.radians(this.angle)) * this.moveSpeed;
    if(this.check == 0){
        for(var i = 0; i < enemies.length; i++){
            if(euclideanDistance(this.position[0], this.position[2], enemies[i].position[0], enemies[i].position[2]) <= 0.5){
                // Delete the bullet
                deleteBulletP(this.index);
    
                // Cause damage to the enemy
                enemies[i].hit(25);
    
                // If the enemy is dead delete it
                if(enemies[i].hp == 0){
                    enemies[i].kill(i);
                }
            }
        }
    
        if(pf.collides(this.position[2], this.position[0], 0.2, 0.2, this.angle, true) != -1){
            deleteBulletP(this.index);
        }

        this.check = 3;
    }
    this.check--;
}

function deleteBulletP(index){
    playerBullets.splice(index, 1);

    for(var i = 0; i < playerBullets.length; i++){
        playerBullets[i].changeIndex(i);
    }
}

function euclideanDistance(x1, y1, x2, y2){
    return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
}

playerBullet.prototype.drawShadow = function(shader) {
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(this.angle));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);	
    
	drawModel(shader, bulletPositionBuffer, -1, -1, -1);
}

playerBullet.prototype.draw = function(shader) {
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(this.angle));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);	
    
	mat4.toInverseMat3(worldMatrix, normalMatrix);
	mat3.transpose(normalMatrix);
    gl.uniformMatrix3fv(shader.normalMatrixUniform, false, normalMatrix);	
    
	drawModel(shader, bulletPositionBuffer, bulletNormalBuffer, bulletTextureBuffer, bulletDiffuse);
}