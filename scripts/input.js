// Input variables
var previousPressedKeys = new Array(256).fill(false);
var currentlyPressedKeys = new Array(256).fill(false);
var previousMouse = false;
var currentMouse = false;
var previousMouseX = -1;
var previousMouseY = -1;
var currentMouseX = -1;
var currentMouseY = -1;

// Input constants
const vk_backspace = 8;
const vk_tab = 9;
const vk_enter = 13;
const vk_shift = 16;
const vk_control = 17;
const vk_alt = 18;
const vk_escape = 27;
const vk_space = 32;
const vk_up = 38;
const vk_down = 40;
const vk_left = 37;
const vk_right = 39;
const vk_delete = 46;
const vk_0 = 48;
const vk_1 = 49;
const vk_2 = 50;
const vk_3 = 51;
const vk_4 = 52;
const vk_5 = 53;
const vk_6 = 54;
const vk_7 = 55;
const vk_8 = 56;
const vk_9 = 57;

// Create object that hold functions
input = {};
input.mouseX = 0;
input.mouseY = 0;

var camera_index = 5;

camera_X = 0.0;
camera_Y = 10.0;
camera_Z = 5.0;
camera_index = 5;

// Input functions
function vk(key) {
	return key.charCodeAt();
}

input.getKey = function (key) {
	return currentlyPressedKeys[key];
}

input.getKeyDown = function (key) {
	return (previousPressedKeys[key] == false && currentlyPressedKeys[key] == true);
}

input.getKeyUp = function (key) {
	return (previousPressedKeys[key] == true && currentlyPressedKeys[key] == false);
}

function handleKeyDown(event) {
	currentlyPressedKeys[event.keyCode] = true;
}

function handleKeyUp(event) {
	currentlyPressedKeys[event.keyCode] = false;
}

input.getMouse = function () {
	return currentMouse;
}

input.getMouseDown = function () {
	return (previousMouse == false && currentMouse == true);
}

input.getMouseUp = function () {
	return (previousMouse == true && currentMouse == false);
}

function updateInput() {
	previousPressedKeys = currentlyPressedKeys.slice();
	previousMouseX = currentMouseX;
	previousMouseY = currentMouseY;
	previousMouse = currentMouse;
}

function handleMouseDown(event) {
	currentMouse = true;
}

function handleMouseUp(event) {
	currentMouse = false;
}

function handleMouseMove(event) {
	input.mouseX = event.clientX - canvas.getBoundingClientRect().left;
	input.mouseY = event.clientY - canvas.getBoundingClientRect().top;
}

/** This is high-level function.
 * It must react to delta being more/less than zero.
 */
function handle(delta) {
	if (delta < 0){
		console.log("DOWN");
		if(camera_index < 11.0){
			camera_Z++;
			camera_Y -= 0.5;
			camera_index++;
		}
		if(camera_index >= 11.0 && camera_index < 17){
			camera_Z -= 1;
			camera_Y -= 0.5;
			camera_index++;
		}
		cameraOffset = [0.0, camera_Y, camera_Z];
	}

	else{
		console.log("UP");
		if(camera_index > 11.0 && camera_index <= 17){
			camera_Z += 1;
			camera_Y += 0.5;
			camera_index--;
		}
		else if(camera_index >= 4.0){
			camera_Z--;
			camera_Y += 0.5;
			camera_index--;
		}
		cameraOffset = [0.0, camera_Y, camera_Z];
	}
}

/** Event handler for mouse wheel event.
*/
function wheel(event){
	var delta = 0;
	if (!event) /* For IE. */
			event = window.event;
	if (event.wheelDelta) { /* IE/Opera. */
			delta = event.wheelDelta/120;
	} else if (event.detail) { /** Mozilla case. */
			/** In Mozilla, sign of delta is different than in IE.
			 * Also, delta is multiple of 3.
			 */
			delta = -event.detail/3;
	}
	/** If delta is nonzero, handle it.
	 * Basically, delta is now positive if wheel was scrolled up,
	 * and negative, if wheel was scrolled down.
	 */
	if (delta)
			handle(delta);
	/** Prevent default actions caused by mouse wheel.
	 * That might be ugly, but we handle scrolls somehow
	 * anyway, so don't bother here..
	 */
	if (event.preventDefault)
			event.preventDefault();
event.returnValue = false;
}

/** Initialization code. 
* If you use your own event management code, change it as required.
*/
if (window.addEventListener)
	/** DOMMouseScroll is for mozilla. */
	window.addEventListener('DOMMouseScroll', wheel, false);
/** IE/Opera. */
window.onmousewheel = document.onmousewheel = wheel;