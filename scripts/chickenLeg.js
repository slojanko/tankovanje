function chickenLeg(x, y, z, angle) {
    this.position = [x, y, z];

    this.angle = angle;

    this.index = bullets.length;
}

chickenLeg.prototype.changeIndex = function(index){
    this.index = index;
}

chickenLeg.prototype.pickUp = function(){
    // Add 10 health points to the player
    player.hit(-10);

    // Delete the drop
    deleteDrop(this.index);
}

function deleteDrop(index){
    drops.splice(index, 1);

    for(var i = 0; i < drops.length; i++){
        drops[i].changeIndex(i);
    }
}

function euclideanDistance(x1, y1, x2, y2){
    return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
}

chickenLeg.prototype.drawShadow = function(shader) {
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(this.angle));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);	
    
	drawModel(shader, chickenLegPositionBuffer, -1, -1, -1);
}

chickenLeg.prototype.draw = function(shader) {
	mat4.identity(worldMatrix);
	mat4.translate(worldMatrix, this.position);
	mat4.rotateY(worldMatrix, math.radians(this.angle));
    gl.uniformMatrix4fv(shader.worldMatrixUniform, false, worldMatrix);	
    
	mat4.toInverseMat3(worldMatrix, normalMatrix);
	mat3.transpose(normalMatrix);
    gl.uniformMatrix3fv(shader.normalMatrixUniform, false, normalMatrix);	
    
	drawModel(shader, chickenLegPositionBuffer, chickenLegNormalBuffer, chickenLegTextureBuffer, chickenLegDiffuse);
}